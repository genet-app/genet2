use genet_attr::attr::{Attr, AttrTypeSetID, BoundAttr};

#[repr(transparent)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Default)]
pub struct LayerTypeID(pub u32);

pub struct Layer<'a> {
    header: AttrTypeSetID,
    attrs: Vec<Attr>,
    payload: &'a [u8],
}

impl<'a> Layer<'a> {
    pub fn attrs(&self) -> &[Attr] {
        &self.attrs
    }

    pub fn payload(&self) -> &[u8] {
        self.payload
    }
}

#[repr(C)]
#[derive(Clone, Debug, Default)]
pub struct LayerData {
    pub header: AttrTypeSetID,
    pub attrs_start: u64,
    pub attrs_end: u64,
    pub payload_start: u64,
    pub payload_end: u64,
    pub depth: u32,
}

#[derive(Clone, Debug)]
pub struct BoundLayer<'a> {
    headers: &'a [Attr],
    attrs: &'a [Attr],
    payload: &'a [u8],
}

impl<'a> BoundLayer<'a> {
    pub(crate) fn new(headers: &'a [Attr], attrs: &'a [Attr], payload: &'a [u8]) -> Self {
        Self {
            headers,
            attrs,
            payload,
        }
    }

    pub fn attrs(&self) -> impl Iterator<Item = BoundAttr<'a>> {
        let data = self.payload;
        self.headers
            .iter()
            .chain(self.attrs.iter())
            .map(move |attr| BoundAttr::new(attr, data))
    }

    pub fn payload(&self) -> &[u8] {
        self.payload
    }
}
