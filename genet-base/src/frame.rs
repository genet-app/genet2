use crate::layer::{BoundLayer, Layer, LayerData};
use genet_attr::attr::{Attr, AttrTable, BoundAttr};

pub struct Frame<'a> {
    index: u64,
    layers: &'a [LayerData],
    payload: &'a [u8],
    attrs: &'a [Attr],
    table: &'a AttrTable<'a>,
    children: Vec<Layer<'a>>,
}

impl<'a> Frame<'a> {
    pub fn new(
        index: u64,
        layers: &'a [LayerData],
        payload: &'a [u8],
        attrs: &'a [Attr],
        table: &'a AttrTable,
    ) -> Self {
        Self {
            index,
            layers,
            payload,
            attrs,
            table,
            children: Vec::new(),
        }
    }

    pub fn layers(&self) -> impl Iterator<Item = BoundLayer<'a>> {
        let payload = self.payload;
        let attrs = self.attrs;
        let table = self.table;
        let rev = self.layers.iter().rev();
        let depth = rev.clone().next().map(|layer| layer.depth).unwrap_or(0);
        rev.filter(move |layer| depth % layer.depth == 0)
            .map(move |f| {
                let headers = table.get(f.header);
                let attrs = attrs
                    .get(f.attrs_start as usize..f.attrs_end as usize)
                    .unwrap_or(&[]);
                let payload = payload
                    .get(f.payload_start as usize..f.payload_end as usize)
                    .unwrap_or(&[]);
                BoundLayer::new(headers, attrs, payload)
            })
    }

    pub fn add_layer(&mut self, layer: Layer<'a>) {
        self.children.push(layer);
    }

    pub fn attrs(&self) -> impl Iterator<Item = BoundAttr<'a>> {
        self.layers().map(|layer| layer.attrs()).flatten()
    }

    pub fn index(&self) -> u64 {
        self.index
    }

    pub fn payload(&mut self) -> &[u8] {
        self.payload
    }

    pub fn children(self) -> Vec<Layer<'a>> {
        self.children
    }
}

pub struct FrameData {
    pub index: u64,
    pub payload: Vec<u8>,
    pub attrs: Vec<Attr>,
    pub layers: Vec<LayerData>,
}

impl FrameData {
    pub fn index(&self) -> u64 {
        self.index
    }
}
