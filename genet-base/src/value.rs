use std::borrow::Cow;

#[derive(Debug, Clone, PartialEq)]
pub enum Value<'a> {
    Nil,
    Bool(bool),
    Int64(i64),
    UInt64(u64),
    Float64(f64),
    Bytes(Cow<'a, [u8]>),
}
