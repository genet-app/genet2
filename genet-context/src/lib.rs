use core::marker::PhantomData;
use failure::Error;
use genet_attr::{
    attr::{AttrType, AttrTypeID, AttrTypeSetID},
    parser::ParserBridge,
};
use genet_base::decoder::DecoderStackID;
use genet_token::Token;
use std::{mem, result, slice, str};

pub type Result<T> = result::Result<T, Error>;

pub trait Context {
    fn get_token(&mut self, id: &str) -> Token;
    fn get_decoder(&mut self, id: &str) -> Result<DecoderStackID>;
    fn register_attr_type(&mut self, attr: &AttrType) -> AttrTypeID;
    fn register_attr_type_set(&mut self, attrs: &[AttrTypeID]) -> AttrTypeSetID;
}

#[repr(transparent)]
#[derive(Clone, Copy)]
struct ContextFatPtr([*mut u8; 2]);

#[repr(C)]
pub struct ContextBridge<'a> {
    phantom: PhantomData<&'a dyn Context>,
    ctx: ContextFatPtr,
    get_token: unsafe extern "C" fn(ContextFatPtr, *const u8, u64) -> Token,
    register_attr_type:
        unsafe extern "C" fn(ContextFatPtr, *const ParserBridge, *const u8, u64) -> AttrTypeID,
    register_attr_type_set:
        unsafe extern "C" fn(ContextFatPtr, *const AttrTypeID, u64) -> AttrTypeSetID,
}

impl<'a> ContextBridge<'a> {
    pub fn new(ctx: &mut dyn Context) -> Self {
        Self {
            phantom: PhantomData,
            ctx: unsafe { mem::transmute(ctx) },
            get_token: context_get_token,
            register_attr_type: context_register_attr_type,
            register_attr_type_set: context_register_attr_type_set,
        }
    }
}

impl<'a> Context for ContextBridge<'a> {
    fn get_token(&mut self, id: &str) -> Token {
        unsafe { (self.get_token)(self.ctx, id.as_ptr(), id.len() as u64) }
    }

    fn get_decoder(&mut self, _id: &str) -> Result<DecoderStackID> {
        Ok(DecoderStackID(0))
    }

    fn register_attr_type(&mut self, attr: &AttrType) -> AttrTypeID {
        if let Ok(buf) = bincode::serialize(attr) {
            unsafe {
                (self.register_attr_type)(self.ctx, &attr.parser, buf.as_ptr(), buf.len() as u64)
            }
        } else {
            AttrTypeID(0)
        }
    }

    fn register_attr_type_set(&mut self, attrs: &[AttrTypeID]) -> AttrTypeSetID {
        unsafe { (self.register_attr_type_set)(self.ctx, attrs.as_ptr(), attrs.len() as u64) }
    }
}

unsafe extern "C" fn context_get_token(ctx: ContextFatPtr, data: *const u8, len: u64) -> Token {
    let ctx: &mut Context = mem::transmute(ctx);
    let id = str::from_utf8_unchecked(slice::from_raw_parts(data, len as usize));
    ctx.get_token(id)
}

unsafe extern "C" fn context_register_attr_type(
    ctx: ContextFatPtr,
    parser: *const ParserBridge,
    data: *const u8,
    len: u64,
) -> AttrTypeID {
    let ctx: &mut Context = mem::transmute(ctx);
    let data = slice::from_raw_parts(data, len as usize);
    if let Ok(mut attr) = bincode::deserialize::<AttrType>(&data) {
        if !parser.is_null() {
            attr.parser = (*parser).clone();
        }
        ctx.register_attr_type(&attr)
    } else {
        AttrTypeID(0)
    }
}

unsafe extern "C" fn context_register_attr_type_set(
    ctx: ContextFatPtr,
    data: *const AttrTypeID,
    len: u64,
) -> AttrTypeSetID {
    let ctx: &mut Context = mem::transmute(ctx);
    let data = slice::from_raw_parts(data, len as usize);
    ctx.register_attr_type_set(data)
}

#[cfg(test)]
mod tests {
    use crate::{Context, ContextBridge, Result};
    use genet_attr::attr::{AttrType, AttrTypeID, AttrTypeSetID};
    use genet_base::decoder::DecoderStackID;
    use genet_token::Token;

    struct TestContext {}

    impl Context for TestContext {
        fn get_token(&mut self, id: &str) -> Token {
            Token::new(1, id)
        }

        fn get_decoder(&mut self, _id: &str) -> Result<DecoderStackID> {
            Ok(DecoderStackID(0))
        }

        fn register_attr_type(&mut self, _attr: &AttrType) -> AttrTypeID {
            AttrTypeID(0)
        }

        fn register_attr_type_set(&mut self, _attrs: &[AttrTypeID]) -> AttrTypeSetID {
            AttrTypeSetID(0)
        }
    }

    #[test]
    fn get_token() {
        let mut bridge = ContextBridge::new(&mut TestContext {});
        let tk = bridge.get_token("ab");
        assert_eq!(tk.index(), 1);
        assert_eq!(tk.as_str(), "ab");
    }
}
