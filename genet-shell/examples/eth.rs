use genet_derive::{Attr, Package};
use genet_sdk::{
    context::Context,
    decoder::{Decoder, ParallelWorker, Status},
    frame::Frame,
    layer::LayerType,
    result::Result,
};

/// Ethernet
#[derive(Default, Attr)]
struct Eth {
    /// Source Hardware Address
    #[attr(alias = "_.src", kind = "@eth:mac", byte_size = 6)]
    src: Vec<u8>,

    /// Destination Hardware Address
    #[attr(alias = "_.dst", kind = "@eth:mac", byte_size = 6, decode = "x[0..2]")]
    dst: Vec<u8>,

    #[attr(ty = "u8", decode = "x as u16 + 5")]
    len: u16,

    #[attr(
        ty = "[u8]",
        decode = "x[0] as u32 + 5",
        alias = "_.srcxxxxxx",
        kind = "@eth:mac"
    )]
    lenx: Eth2,

    #[attr(ty = "u16", decode = "x + 5")]
    lenty: EthType,
}

#[derive(Default, Attr)]
#[attr(ty = "u32")]
struct Eth2 {
    /// Source Hardware Address
    #[attr(alias = "_.src", kind = "@eth:mac2", byte_size = 6)]
    src: Vec<u8>,

    /// Destination Hardware Address
    #[attr(alias = "_.dst", kind = "@eth:mac2", byte_size = 6, decode = "x[0..2]")]
    dst: Vec<u8>,

    /// Destination Hardware Address
    #[attr(decode = "x + 5")]
    len: u16,
}

#[derive(Attr, Debug)]
#[attr(ty = "u16")]
enum EthType {
    IPv4,
    ARP,
    WOL,
    IPv6,
    EAP,
    Unknown,
}

impl Default for EthType {
    fn default() -> Self {
        EthType::Unknown
    }
}

impl From<u16> for EthType {
    fn from(data: u16) -> Self {
        match data {
            0x0800 => EthType::IPv4,
            0x0806 => EthType::ARP,
            0x0842 => EthType::WOL,
            0x86DD => EthType::IPv6,
            0x888E => EthType::EAP,
            _ => Self::default(),
        }
    }
}

struct Worker {
    eth: LayerType<Eth>,
    //ipv4: DecoderStackID,
}

impl ParallelWorker for Worker {
    fn decode(&self, _frame: &mut Frame) -> Result<Status> {
        Ok(Status::Done)
    }
}

#[derive(Default)]
struct EthDecoder {}

impl Decoder for EthDecoder {
    fn parallel_worker(&self, ctx: &mut dyn Context) -> Result<Box<dyn ParallelWorker>> {
        Ok(Box::new(Worker {
            eth: LayerType::new(ctx, "eth", Default::default()),
            //ipv4: ctx.get_decoder("app.genet.decoder.ipv4")?,
        }))
    }
}

#[derive(Default, Package)]
struct Package {
    #[decoder(id = "app.genet.decoder.eth")]
    decoder: EthDecoder,
}

#[cfg(test)]
mod tests {
    use super::Eth;
    use genet_chamber::{context::ChamberContext, profile::Profile};
    use genet_sdk::layer::LayerType;

    #[test]
    fn nil() {
        let profile = Profile::new();
        let mut ctx = ChamberContext::new(profile);
        let eth: LayerType<Eth> = LayerType::new(&mut ctx, "eth", Default::default());
        //eth.get_range(&eth.len, 0..0);
    }
}
