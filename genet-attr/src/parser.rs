use crate::{
    attr::Attr,
    result::Result,
    value::{OwnedValue, Value},
};
use failure::err_msg;
use std::{borrow::Cow, fmt, mem, slice, str};

#[repr(i8)]
enum ValueID {
    Err = -1,
    Nil = 0,
    Bool = 1,
    Int8 = 2,
    Int16 = 3,
    Int32 = 4,
    Int64 = 5,
    UInt8 = 6,
    UInt16 = 7,
    UInt32 = 8,
    UInt64 = 9,
    Float32 = 10,
    Float64 = 11,
    Bytes = 12,
}

#[repr(C)]
union ValueUnion {
    int64: i64,
    uint64: u64,
    float64: f64,
    data: BytesData,
}

#[repr(C)]
#[derive(Clone, Copy)]
struct BytesData {
    data: *const u8,
    len: u64,
    owned: u8,
}

type ResultPtr = *mut ();
type ResultCallback = unsafe extern "C" fn(out: ResultPtr, value: ValueUnion, id: ValueID);

pub trait Parser: Send + Sync {
    fn parse(&self, attr: &Attr, data: &[u8]) -> Result<OwnedValue>;
    fn box_clone(&self) -> Box<dyn Parser>;
}

impl fmt::Debug for dyn Parser {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Parser")
    }
}

impl Clone for Box<dyn Parser> {
    fn clone(&self) -> Box<dyn Parser> {
        self.box_clone()
    }
}

pub struct ParserFunc<F>(F);

impl<F: 'static + Fn(&Attr, &[u8]) -> Result<OwnedValue> + Send + Sync + Clone> ParserFunc<F> {
    pub fn wrap(func: F) -> Box<Parser> {
        Box::new(ParserFunc(func))
    }
}

impl<F: 'static + Fn(&Attr, &[u8]) -> Result<OwnedValue> + Send + Sync + Clone> Parser
    for ParserFunc<F>
{
    fn parse(&self, attr: &Attr, data: &[u8]) -> Result<OwnedValue> {
        (self.0)(attr, data)
    }

    fn box_clone(&self) -> Box<Parser> {
        Box::new(ParserFunc(self.0.clone()))
    }
}

#[repr(C)]
pub struct ParserBridge {
    mapper: *mut Box<dyn Parser>,
    map: unsafe extern "C" fn(
        *const Box<dyn Parser>,
        *const Attr,
        *const u8,
        u64,
        ResultPtr,
        ResultCallback,
    ),
    clone: unsafe extern "C" fn(*mut Box<dyn Parser>) -> ParserBridge,
    drop: unsafe extern "C" fn(*mut Box<dyn Parser>),
}

unsafe impl Send for ParserBridge {}

impl fmt::Debug for ParserBridge {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ParserBridge")
    }
}

impl Default for ParserBridge {
    fn default() -> Self {
        Self::new(ParserFunc::wrap(|_, _| Err(err_msg("unimplemented"))))
    }
}

impl ParserBridge {
    pub fn new(mapper: Box<dyn Parser>) -> Self {
        Self {
            mapper: Box::into_raw(Box::new(mapper)),
            map: mapper_map,
            clone: mapper_clone,
            drop: mapper_drop,
        }
    }

    pub fn parse(&self, attr: &Attr, data: &[u8]) -> Result<Value> {
        let mut result = Ok(Value::Nil);
        unsafe extern "C" fn cb(out: ResultPtr, value: ValueUnion, id: ValueID) {
            let res: *mut Result<Value> = mem::transmute(out);
            let res = &mut *res;
            match id {
                ValueID::Err => {
                    let err = str::from_utf8_unchecked(slice::from_raw_parts(
                        value.data.data,
                        value.data.len as usize,
                    ));
                    *res = Err(err_msg(err));
                }
                ValueID::Nil => *res = Ok(Value::Nil),
                ValueID::Bool => *res = Ok(Value::Bool(value.int64 != 0)),
                ValueID::Int8 => *res = Ok(Value::Int8(value.int64 as i8)),
                ValueID::Int16 => *res = Ok(Value::Int16(value.int64 as i16)),
                ValueID::Int32 => *res = Ok(Value::Int32(value.int64 as i32)),
                ValueID::Int64 => *res = Ok(Value::Int64(value.int64)),
                ValueID::UInt8 => *res = Ok(Value::UInt8(value.uint64 as u8)),
                ValueID::UInt16 => *res = Ok(Value::UInt16(value.uint64 as u16)),
                ValueID::UInt32 => *res = Ok(Value::UInt32(value.uint64 as u32)),
                ValueID::UInt64 => *res = Ok(Value::UInt64(value.uint64)),
                ValueID::Float32 => *res = Ok(Value::Float32(value.float64 as f32)),
                ValueID::Float64 => *res = Ok(Value::Float64(value.float64)),
                ValueID::Bytes => {
                    let sub = slice::from_raw_parts(value.data.data, value.data.len as usize);
                    *res = Ok(Value::Bytes(if value.data.owned == 1 {
                        Cow::Owned(sub.to_owned())
                    } else {
                        Cow::Borrowed(sub)
                    }));
                }
            }
        }
        unsafe {
            (self.map)(
                self.mapper,
                attr,
                data.as_ptr(),
                data.len() as u64,
                &mut result as *mut Result<Value> as *mut (),
                cb,
            );
        }
        result
    }
}

impl Clone for ParserBridge {
    fn clone(&self) -> Self {
        unsafe { (self.clone)(self.mapper) }
    }
}

impl Drop for ParserBridge {
    fn drop(&mut self) {
        unsafe { (self.drop)(self.mapper) };
    }
}

unsafe extern "C" fn mapper_map(
    mapper: *const Box<dyn Parser>,
    attr: *const Attr,
    data: *const u8,
    len: u64,
    res: ResultPtr,
    res_cb: ResultCallback,
) {
    let mapper = &*mapper;
    let attr = &*attr;
    let data = slice::from_raw_parts(data, len as usize);
    match mapper.parse(attr, data).map(|v| v.into_value(data)) {
        Ok(Value::Nil) => res_cb(res, ValueUnion { int64: 0 }, ValueID::Nil),
        Ok(Value::Bool(b)) => res_cb(
            res,
            ValueUnion {
                int64: if b { 1 } else { 0 },
            },
            ValueID::Bool,
        ),
        Ok(Value::Int8(i)) => res_cb(
            res,
            ValueUnion {
                int64: i64::from(i),
            },
            ValueID::Int8,
        ),
        Ok(Value::Int16(i)) => res_cb(
            res,
            ValueUnion {
                int64: i64::from(i),
            },
            ValueID::Int16,
        ),
        Ok(Value::Int32(i)) => res_cb(
            res,
            ValueUnion {
                int64: i64::from(i),
            },
            ValueID::Int32,
        ),
        Ok(Value::Int64(i)) => res_cb(res, ValueUnion { int64: i }, ValueID::Int64),
        Ok(Value::UInt8(u)) => res_cb(
            res,
            ValueUnion {
                uint64: u64::from(u),
            },
            ValueID::UInt8,
        ),
        Ok(Value::UInt16(u)) => res_cb(
            res,
            ValueUnion {
                uint64: u64::from(u),
            },
            ValueID::UInt16,
        ),
        Ok(Value::UInt32(u)) => res_cb(
            res,
            ValueUnion {
                uint64: u64::from(u),
            },
            ValueID::UInt32,
        ),
        Ok(Value::UInt64(u)) => res_cb(res, ValueUnion { uint64: u }, ValueID::UInt64),
        Ok(Value::Float32(f)) => res_cb(
            res,
            ValueUnion {
                float64: f64::from(f),
            },
            ValueID::Float32,
        ),
        Ok(Value::Float64(f)) => res_cb(res, ValueUnion { float64: f }, ValueID::Float64),
        Ok(Value::Bytes(b)) => {
            res_cb(
                res,
                ValueUnion {
                    data: BytesData {
                        data: b.as_ptr(),
                        len: b.len() as u64,
                        owned: match b {
                            Cow::Borrowed(_) => 0,
                            Cow::Owned(_) => 1,
                        },
                    },
                },
                ValueID::Bytes,
            );
        }
        Err(e) => {
            let s = format!("{}", e);
            res_cb(
                res,
                ValueUnion {
                    data: BytesData {
                        data: s.as_ptr(),
                        len: s.len() as u64,
                        owned: 1,
                    },
                },
                ValueID::Err,
            );
        }
    }
}

unsafe extern "C" fn mapper_clone(mapper: *mut Box<dyn Parser>) -> ParserBridge {
    ParserBridge::new((*mapper).clone())
}

unsafe extern "C" fn mapper_drop(mapper: *mut Box<dyn Parser>) {
    Box::from_raw(mapper);
}

#[cfg(test)]
mod tests {
    use crate::{
        attr::Attr,
        parser::{ParserBridge, ParserFunc},
        value::{IntoOwnedValue, Value},
    };
    use std::borrow::Cow;

    #[test]
    fn nil() {
        let mapper = ParserBridge::new(ParserFunc::wrap(|_attr, data| {
            Ok(Value::Nil.into_owned_value(data))
        }));
        assert_eq!(mapper.parse(&Attr::default(), &[]).ok(), Some(Value::Nil));
    }

    #[test]
    fn bool() {
        let mapper = ParserBridge::new(ParserFunc::wrap(|_attr, data| {
            Ok(Value::Bool(true).into_owned_value(data))
        }));
        assert_eq!(
            mapper.parse(&Attr::default(), &[]).ok(),
            Some(Value::Bool(true))
        );
        let mapper = ParserBridge::new(ParserFunc::wrap(|_attr, data| {
            Ok(Value::Bool(false).into_owned_value(data))
        }));
        assert_eq!(
            mapper.parse(&Attr::default(), &[]).ok(),
            Some(Value::Bool(false))
        );
    }

    #[test]
    fn int64() {
        let mapper = ParserBridge::new(ParserFunc::wrap(|_attr, data| {
            Ok(Value::Int64(-1234).into_owned_value(data))
        }));
        assert_eq!(
            mapper.parse(&Attr::default(), &[]).ok(),
            Some(Value::Int64(-1234))
        );
    }

    #[test]
    fn uint64() {
        let mapper = ParserBridge::new(ParserFunc::wrap(|_attr, data| {
            Ok(Value::UInt64(1234).into_owned_value(data))
        }));
        assert_eq!(
            mapper.parse(&Attr::default(), &[]).ok(),
            Some(Value::UInt64(1234))
        );
    }

    #[test]
    fn float64() {
        let mapper = ParserBridge::new(ParserFunc::wrap(|_attr, data| {
            Ok(Value::Float64(1234.5678).into_owned_value(data))
        }));
        assert_eq!(
            mapper.parse(&Attr::default(), &[]).ok(),
            Some(Value::Float64(1234.5678))
        );
    }

    #[test]
    fn byte() {
        let mapper = ParserBridge::new(ParserFunc::wrap(|_attr, data| {
            Ok(Into::<Value>::into(data.to_owned()).into_owned_value(data))
        }));
        let data = vec![0, 1, 2, 3, 4];
        assert_eq!(
            mapper.parse(&Attr::default(), &[0, 1, 2, 3, 4]).ok(),
            Some(Value::Bytes(Cow::Owned(data)))
        );
        let mapper = ParserBridge::new(ParserFunc::wrap(|_attr, data| {
            Ok(Into::<Value>::into(data).into_owned_value(data))
        }));
        let data = vec![0, 1, 2, 3, 4];
        assert_eq!(
            mapper.parse(&Attr::default(), &[0, 1, 2, 3, 4]).ok(),
            Some(Value::Bytes(Cow::Borrowed(&data)))
        );
    }
}
