use crate::parser::ParserBridge;
use genet_token::Token;
use serde_derive::{Deserialize, Serialize};
use std::ops::Range;

#[repr(C)]
#[derive(Clone, Debug, Default)]
pub struct Attr {
    pub ty: AttrTypeID,
    pub id: Token,
    pub alias: Token,
    pub kind: Token,
    pub bit_range_start: u64,
    pub bit_range_end: u64,
}

impl Attr {
    pub fn bit_range(&self) -> Range<usize> {
        self.bit_range_start as usize..self.bit_range_end as usize
    }

    pub fn byte_range(&self) -> Range<usize> {
        let range = self.bit_range();
        (range.start / 8)..((range.end + 7) / 8)
    }
}

#[derive(Clone, Debug)]
pub struct BoundAttr<'a> {
    attr: &'a Attr,
    data: &'a [u8],
}

impl<'a> BoundAttr<'a> {
    pub fn new(attr: &'a Attr, data: &'a [u8]) -> Self {
        Self { attr, data }
    }
}

#[repr(transparent)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Default)]
pub struct AttrTypeID(pub u32);

#[repr(transparent)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Default)]
pub struct AttrTypeSetID(pub u32);

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct AttrType {
    pub index: Option<usize>,
    pub id: String,
    pub alias: String,
    pub kind: String,
    pub bit_range: Range<usize>,
    pub name: String,
    pub description: String,

    #[serde(skip)]
    pub parser: ParserBridge,
}

impl Default for AttrType {
    fn default() -> Self {
        Self {
            index: Default::default(),
            id: Default::default(),
            alias: Default::default(),
            kind: Default::default(),
            bit_range: 0..0,
            name: Default::default(),
            description: Default::default(),
            parser: Default::default(),
        }
    }
}

#[derive(Clone)]
pub struct AttrTable<'a> {
    attrs: &'a [Attr],
    entries: &'a [AttrTableEntry],
}

impl<'a> AttrTable<'a> {
    pub fn new(attrs: &'a [Attr], entries: &'a [AttrTableEntry]) -> Self {
        Self { attrs, entries }
    }

    pub fn get(&self, index: AttrTypeSetID) -> &[Attr] {
        self.entries
            .get(index.0 as usize)
            .and_then(|e| self.attrs.get(e.start as usize..e.end as usize))
            .unwrap_or(&[])
    }
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct AttrTableEntry {
    pub start: u64,
    pub end: u64,
}
