use std::borrow::Cow;

#[derive(Debug, Clone, PartialEq)]
pub enum Value<'a> {
    Nil,
    Bool(bool),
    Int8(i8),
    Int16(i16),
    Int32(i32),
    Int64(i64),
    UInt8(u8),
    UInt16(u16),
    UInt32(u32),
    UInt64(u64),
    Float32(f32),
    Float64(f64),
    Bytes(Cow<'a, [u8]>),
}

impl<'a> AsRef<bool> for Value<'a> {
    fn as_ref(&self) -> &bool {
        match self {
            Value::Bool(v) => v,
            _ => &false,
        }
    }
}

impl<'a> AsRef<u8> for Value<'a> {
    fn as_ref(&self) -> &u8 {
        match self {
            Value::UInt8(v) => v,
            _ => &0,
        }
    }
}

impl<'a> AsRef<u16> for Value<'a> {
    fn as_ref(&self) -> &u16 {
        match self {
            Value::UInt16(v) => v,
            _ => &0,
        }
    }
}

impl<'a> AsRef<u32> for Value<'a> {
    fn as_ref(&self) -> &u32 {
        match self {
            Value::UInt32(v) => v,
            _ => &0,
        }
    }
}

impl<'a> AsRef<u64> for Value<'a> {
    fn as_ref(&self) -> &u64 {
        match self {
            Value::UInt64(v) => v,
            _ => &0,
        }
    }
}

impl<'a> AsRef<i8> for Value<'a> {
    fn as_ref(&self) -> &i8 {
        match self {
            Value::Int8(v) => v,
            _ => &0,
        }
    }
}

impl<'a> AsRef<i16> for Value<'a> {
    fn as_ref(&self) -> &i16 {
        match self {
            Value::Int16(v) => v,
            _ => &0,
        }
    }
}

impl<'a> AsRef<i32> for Value<'a> {
    fn as_ref(&self) -> &i32 {
        match self {
            Value::Int32(v) => v,
            _ => &0,
        }
    }
}

impl<'a> AsRef<i64> for Value<'a> {
    fn as_ref(&self) -> &i64 {
        match self {
            Value::Int64(v) => v,
            _ => &0,
        }
    }
}

impl<'a> AsRef<f32> for Value<'a> {
    fn as_ref(&self) -> &f32 {
        match self {
            Value::Float32(v) => v,
            _ => &0.0,
        }
    }
}

impl<'a> AsRef<f64> for Value<'a> {
    fn as_ref(&self) -> &f64 {
        match self {
            Value::Float64(v) => v,
            _ => &0.0,
        }
    }
}

impl<'a> AsRef<[u8]> for Value<'a> {
    fn as_ref(&self) -> &[u8] {
        match self {
            Value::Bytes(Cow::Borrowed(v)) => v,
            Value::Bytes(Cow::Owned(v)) => &v,
            _ => &[],
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum OwnedValue {
    Nil,
    Bool(bool),
    Int8(i8),
    Int16(i16),
    Int32(i32),
    Int64(i64),
    UInt8(u8),
    UInt16(u16),
    UInt32(u32),
    UInt64(u64),
    Float32(f32),
    Float64(f64),
    Bytes(usize, usize),
    OwnedBytes(Vec<u8>),
}

impl OwnedValue {
    pub fn into_value(self, data: &[u8]) -> Value {
        match self {
            OwnedValue::Nil => Value::Nil,
            OwnedValue::Bool(b) => Value::Bool(b),
            OwnedValue::Int8(i) => Value::Int8(i),
            OwnedValue::Int16(i) => Value::Int16(i),
            OwnedValue::Int32(i) => Value::Int32(i),
            OwnedValue::Int64(i) => Value::Int64(i),
            OwnedValue::UInt8(u) => Value::UInt8(u),
            OwnedValue::UInt16(u) => Value::UInt16(u),
            OwnedValue::UInt32(u) => Value::UInt32(u),
            OwnedValue::UInt64(u) => Value::UInt64(u),
            OwnedValue::Float32(f) => Value::Float32(f),
            OwnedValue::Float64(f) => Value::Float64(f),
            OwnedValue::Bytes(start, end) => {
                Value::Bytes(Cow::Borrowed(data.get(start..end).unwrap_or(&[])))
            }
            OwnedValue::OwnedBytes(b) => Value::Bytes(Cow::Owned(b)),
        }
    }
}

impl Into<bool> for OwnedValue {
    fn into(self) -> bool {
        match self {
            OwnedValue::Bool(v) => v,
            _ => Default::default(),
        }
    }
}

impl Into<u8> for OwnedValue {
    fn into(self) -> u8 {
        match self {
            OwnedValue::UInt8(v) => v,
            _ => Default::default(),
        }
    }
}

impl Into<u16> for OwnedValue {
    fn into(self) -> u16 {
        match self {
            OwnedValue::UInt16(v) => v,
            _ => Default::default(),
        }
    }
}

impl Into<u32> for OwnedValue {
    fn into(self) -> u32 {
        match self {
            OwnedValue::UInt32(v) => v,
            _ => Default::default(),
        }
    }
}

impl Into<u64> for OwnedValue {
    fn into(self) -> u64 {
        match self {
            OwnedValue::UInt64(v) => v,
            _ => Default::default(),
        }
    }
}

impl Into<i8> for OwnedValue {
    fn into(self) -> i8 {
        match self {
            OwnedValue::Int8(v) => v,
            _ => Default::default(),
        }
    }
}

impl Into<i16> for OwnedValue {
    fn into(self) -> i16 {
        match self {
            OwnedValue::Int16(v) => v,
            _ => Default::default(),
        }
    }
}

impl Into<i32> for OwnedValue {
    fn into(self) -> i32 {
        match self {
            OwnedValue::Int32(v) => v,
            _ => Default::default(),
        }
    }
}

impl Into<i64> for OwnedValue {
    fn into(self) -> i64 {
        match self {
            OwnedValue::Int64(v) => v,
            _ => Default::default(),
        }
    }
}

impl Into<f32> for OwnedValue {
    fn into(self) -> f32 {
        match self {
            OwnedValue::Float32(v) => v,
            _ => Default::default(),
        }
    }
}

impl Into<f64> for OwnedValue {
    fn into(self) -> f64 {
        match self {
            OwnedValue::Float64(v) => v,
            _ => Default::default(),
        }
    }
}

impl Into<Vec<u8>> for OwnedValue {
    fn into(self) -> Vec<u8> {
        match self {
            OwnedValue::OwnedBytes(v) => v,
            _ => Default::default(),
        }
    }
}

pub trait IntoOwnedValue {
    fn into_owned_value(self, data: &[u8]) -> OwnedValue;
}

impl IntoOwnedValue for bool {
    fn into_owned_value(self, _data: &[u8]) -> OwnedValue {
        OwnedValue::Bool(self)
    }
}

impl IntoOwnedValue for u8 {
    fn into_owned_value(self, _data: &[u8]) -> OwnedValue {
        OwnedValue::UInt8(self)
    }
}

impl IntoOwnedValue for u16 {
    fn into_owned_value(self, _data: &[u8]) -> OwnedValue {
        OwnedValue::UInt16(self)
    }
}

impl IntoOwnedValue for u32 {
    fn into_owned_value(self, _data: &[u8]) -> OwnedValue {
        OwnedValue::UInt32(self)
    }
}

impl IntoOwnedValue for u64 {
    fn into_owned_value(self, _data: &[u8]) -> OwnedValue {
        OwnedValue::UInt64(self)
    }
}

impl IntoOwnedValue for i8 {
    fn into_owned_value(self, _data: &[u8]) -> OwnedValue {
        OwnedValue::Int8(self)
    }
}

impl IntoOwnedValue for i16 {
    fn into_owned_value(self, _data: &[u8]) -> OwnedValue {
        OwnedValue::Int16(self)
    }
}

impl IntoOwnedValue for i32 {
    fn into_owned_value(self, _data: &[u8]) -> OwnedValue {
        OwnedValue::Int32(self)
    }
}

impl IntoOwnedValue for i64 {
    fn into_owned_value(self, _data: &[u8]) -> OwnedValue {
        OwnedValue::Int64(self)
    }
}

impl IntoOwnedValue for f32 {
    fn into_owned_value(self, _data: &[u8]) -> OwnedValue {
        OwnedValue::Float32(self)
    }
}

impl IntoOwnedValue for f64 {
    fn into_owned_value(self, _data: &[u8]) -> OwnedValue {
        OwnedValue::Float64(self)
    }
}

impl IntoOwnedValue for &[u8] {
    fn into_owned_value(self, data: &[u8]) -> OwnedValue {
        if data.as_ptr() <= self.as_ptr() {
            let start = self.as_ptr() as usize - data.as_ptr() as usize;
            let end = start + self.len();
            if end < data.as_ptr() as usize {
                return OwnedValue::Bytes(start, end);
            }
        }
        OwnedValue::Bytes(0, 0)
    }
}

impl IntoOwnedValue for Vec<u8> {
    fn into_owned_value(self, _data: &[u8]) -> OwnedValue {
        OwnedValue::OwnedBytes(self)
    }
}

impl<'a> IntoOwnedValue for Value<'a> {
    fn into_owned_value(self, data: &[u8]) -> OwnedValue {
        match self {
            Value::Nil => OwnedValue::Nil,
            Value::Bool(b) => OwnedValue::Bool(b),
            Value::Int8(i) => OwnedValue::Int8(i),
            Value::Int16(i) => OwnedValue::Int16(i),
            Value::Int32(i) => OwnedValue::Int32(i),
            Value::Int64(i) => OwnedValue::Int64(i),
            Value::UInt8(u) => OwnedValue::UInt8(u),
            Value::UInt16(u) => OwnedValue::UInt16(u),
            Value::UInt32(u) => OwnedValue::UInt32(u),
            Value::UInt64(u) => OwnedValue::UInt64(u),
            Value::Float32(f) => OwnedValue::Float32(f),
            Value::Float64(f) => OwnedValue::Float64(f),
            Value::Bytes(Cow::Borrowed(b)) => {
                if data.as_ptr() <= b.as_ptr() {
                    let start = b.as_ptr() as usize - data.as_ptr() as usize;
                    let end = start + b.len();
                    if end < data.as_ptr() as usize {
                        return OwnedValue::Bytes(start, end);
                    }
                }
                OwnedValue::Bytes(0, 0)
            }
            Value::Bytes(Cow::Owned(b)) => OwnedValue::OwnedBytes(b),
        }
    }
}

impl<'a> Into<Value<'a>> for bool {
    fn into(self) -> Value<'a> {
        Value::Bool(self)
    }
}

impl<'a> Into<Value<'a>> for i8 {
    fn into(self) -> Value<'a> {
        Value::Int64(i64::from(self))
    }
}

impl<'a> Into<Value<'a>> for i16 {
    fn into(self) -> Value<'a> {
        Value::Int64(i64::from(self))
    }
}

impl<'a> Into<Value<'a>> for i32 {
    fn into(self) -> Value<'a> {
        Value::Int64(i64::from(self))
    }
}

impl<'a> Into<Value<'a>> for i64 {
    fn into(self) -> Value<'a> {
        Value::Int64(self)
    }
}

impl<'a> Into<Value<'a>> for u8 {
    fn into(self) -> Value<'a> {
        Value::UInt64(u64::from(self))
    }
}

impl<'a> Into<Value<'a>> for u16 {
    fn into(self) -> Value<'a> {
        Value::UInt64(u64::from(self))
    }
}

impl<'a> Into<Value<'a>> for u32 {
    fn into(self) -> Value<'a> {
        Value::UInt64(u64::from(self))
    }
}

impl<'a> Into<Value<'a>> for u64 {
    fn into(self) -> Value<'a> {
        Value::UInt64(self)
    }
}

impl<'a> Into<Value<'a>> for f32 {
    fn into(self) -> Value<'a> {
        Value::Float64(f64::from(self))
    }
}

impl<'a> Into<Value<'a>> for f64 {
    fn into(self) -> Value<'a> {
        Value::Float64(self)
    }
}

impl<'a> Into<Value<'a>> for &'a [u8] {
    fn into(self) -> Value<'a> {
        Value::Bytes(Cow::Borrowed(self))
    }
}

impl<'a> Into<Value<'a>> for Vec<u8> {
    fn into(self) -> Value<'a> {
        Value::Bytes(Cow::Owned(self))
    }
}

impl<'a> Into<Value<'a>> for Box<[u8]> {
    fn into(self) -> Value<'a> {
        Value::Bytes(Cow::Owned(self.into()))
    }
}
