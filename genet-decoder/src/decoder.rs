use crate::{
    result::Result,
    worker::{ParallelWorker, SerialWorker},
};
use failure::err_msg;
use genet_context::Context;

pub trait Decoder {
    fn parallel_worker(&self, _ctx: &mut dyn Context) -> Result<Box<dyn ParallelWorker>>;
    fn serial_worker(&self, _ctx: &mut dyn Context) -> Result<Box<dyn SerialWorker>> {
        Err(err_msg("not yet implemented"))
    }
}
