use genet_base::decoder::DecoderStackID;

pub enum Status {
    Done,
    NextDecoder(DecoderStackID),
}
