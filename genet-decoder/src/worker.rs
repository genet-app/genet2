use crate::{result::Result, status::Status};
use genet_base::frame::Frame;

pub trait ParallelWorker {
    fn decode(&self, frame: &mut Frame) -> Result<Status>;
}

pub trait SerialWorker {
    fn decode(&mut self, frame: &mut Frame) -> Result<Status>;
}
