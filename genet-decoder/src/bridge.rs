use crate::{
    decoder::Decoder,
    result::Result,
    status::Status,
    worker::{ParallelWorker, SerialWorker},
};
use failure::err_msg;
use genet_attr::attr::{Attr, AttrTable, AttrTableEntry};
use genet_base::{
    decoder::DecoderStackID,
    frame::{Frame, FrameData},
    layer::LayerData,
};
use genet_context::{Context, ContextBridge};
use std::{mem, ops::Range, ptr, slice, str};

type ErrPtr = *mut Option<String>;
type ErrCallback = unsafe extern "C" fn(out: ErrPtr, data: *const u8, len: u64);

type FramePtr = *mut FrameData;
type FrameCallback = unsafe extern "C" fn(out: FramePtr, data: FrameCallbackData);

#[repr(C)]
struct FrameCallbackData {
    payload: *const u8,
    payload_len: u64,
    attrs: *const Attr,
    attrs_len: u64,
}

fn range_from_slice(base: &[u8], slice: &[u8]) -> Range<usize> {
    if base.as_ptr() <= slice.as_ptr() {
        let start = slice.as_ptr() as usize - base.as_ptr() as usize;
        let end = start + slice.len();
        if end < base.as_ptr() as usize {
            return start..end;
        }
    }
    0..0
}

#[derive(Default, Clone)]
pub struct WorkerContext {
    pub table_attrs: Vec<Attr>,
    pub table_entries: Vec<AttrTableEntry>,
}

#[repr(C)]
pub struct DecoderBridge {
    decoder: *mut Box<dyn Decoder>,
    parallel_worker: unsafe extern "C" fn(
        *mut ParallelWorkerBridge,
        *mut Box<dyn Decoder>,
        ContextBridge,
        ErrPtr,
        ErrCallback,
    ),
    serial_worker: unsafe extern "C" fn(
        *mut SerialWorkerBridge,
        *mut Box<dyn Decoder>,
        ContextBridge,
        ErrPtr,
        ErrCallback,
    ),
    drop: unsafe extern "C" fn(*mut Box<dyn Decoder>),
}

impl DecoderBridge {
    pub fn new<T: 'static + Decoder>(decoder: T) -> Self {
        Self {
            decoder: Box::into_raw(Box::new(Box::new(decoder))),
            parallel_worker: decoder_parallel_worker,
            serial_worker: decoder_serial_worker,
            drop: decoder_drop,
        }
    }

    pub fn parallel_worker(&self, ctx: &mut dyn Context) -> Result<ParallelWorkerBridge> {
        unsafe extern "C" fn err_cb(out: ErrPtr, data: *const u8, len: u64) {
            *out = Some(
                str::from_utf8_unchecked(slice::from_raw_parts(data, len as usize)).to_string(),
            )
        }

        let mut worker;
        let mut err: Option<String> = None;

        unsafe {
            worker = mem::uninitialized();
            (self.parallel_worker)(
                &mut worker,
                self.decoder,
                ContextBridge::new(ctx),
                &mut err,
                err_cb,
            );
        }

        if let Some(err) = err {
            mem::forget(worker);
            Err(err_msg(err))
        } else {
            Ok(worker)
        }
    }

    pub fn serial_worker(&self, ctx: &mut dyn Context) -> Result<SerialWorkerBridge> {
        unsafe extern "C" fn err_cb(out: ErrPtr, data: *const u8, len: u64) {
            *out = Some(
                str::from_utf8_unchecked(slice::from_raw_parts(data, len as usize)).to_string(),
            )
        }

        let mut worker;
        let mut err: Option<String> = None;

        unsafe {
            worker = mem::uninitialized();
            (self.serial_worker)(
                &mut worker,
                self.decoder,
                ContextBridge::new(ctx),
                &mut err,
                err_cb,
            );
        }

        if let Some(err) = err {
            mem::forget(worker);
            Err(err_msg(err))
        } else {
            Ok(worker)
        }
    }
}

impl Drop for DecoderBridge {
    fn drop(&mut self) {
        unsafe { (self.drop)(self.decoder) };
    }
}

unsafe extern "C" fn decoder_parallel_worker(
    out: *mut ParallelWorkerBridge,
    decoder: *mut Box<dyn Decoder>,
    ctx: ContextBridge,
    err: ErrPtr,
    err_cb: ErrCallback,
) {
    let mut ctx = ctx;
    let decoder = &*decoder;
    match decoder.parallel_worker(&mut ctx) {
        Ok(worker) => {
            ptr::write(out, ParallelWorkerBridge::new(worker));
        }
        Err(e) => {
            let s = format!("{}", e);
            err_cb(err, s.as_ptr(), s.len() as u64);
        }
    }
}

unsafe extern "C" fn decoder_serial_worker(
    out: *mut SerialWorkerBridge,
    decoder: *mut Box<dyn Decoder>,
    ctx: ContextBridge,
    err: ErrPtr,
    err_cb: ErrCallback,
) {
    let mut ctx = ctx;
    let decoder = &*decoder;
    match decoder.serial_worker(&mut ctx) {
        Ok(worker) => {
            ptr::write(out, SerialWorkerBridge::new(worker));
        }
        Err(e) => {
            let s = format!("{}", e);
            err_cb(err, s.as_ptr(), s.len() as u64);
        }
    }
}

unsafe extern "C" fn decoder_drop(decoder: *mut Box<dyn Decoder>) {
    Box::from_raw(decoder);
}

#[repr(C)]
pub struct ParallelWorkerBridge {
    worker: *mut Box<dyn ParallelWorker>,
    decode: unsafe extern "C" fn(
        *const Box<dyn ParallelWorker>,
        FramePod,
        FramePtr,
        FrameCallback,
        ErrPtr,
        ErrCallback,
    ) -> StatusPod,
    drop: unsafe extern "C" fn(*mut Box<dyn ParallelWorker>),
}

impl ParallelWorkerBridge {
    pub fn new(worker: Box<dyn ParallelWorker>) -> Self {
        Self {
            worker: Box::into_raw(Box::new(worker)),
            decode: parallel_worker_decode,
            drop: parallel_worker_drop,
        }
    }

    pub fn decode(&self, frame: &mut FrameData, ctx: &WorkerContext) -> Result<Status> {
        unsafe extern "C" fn data_cb(out: FramePtr, data: FrameCallbackData) {
            let frame = &mut *out;
            let payload = range_from_slice(
                &frame.payload,
                slice::from_raw_parts(data.payload, data.payload_len as usize),
            );
            let mut attrs = slice::from_raw_parts(data.attrs, data.attrs_len as usize).to_vec();
            frame.attrs.append(&mut attrs);
            frame.layers.push(LayerData {
                payload_start: payload.start as u64,
                payload_end: payload.end as u64,
                ..Default::default()
            });
        }

        unsafe extern "C" fn err_cb(out: ErrPtr, data: *const u8, len: u64) {
            *out = Some(
                str::from_utf8_unchecked(slice::from_raw_parts(data, len as usize)).to_string(),
            )
        }

        let mut err: Option<String> = None;
        let data = frame as *mut FrameData;

        let frame = FramePod {
            index: frame.index(),
            payload: frame.payload.as_ptr(),
            payload_len: frame.payload.len() as u64,
            attrs: frame.attrs.as_ptr(),
            attrs_len: frame.attrs.len() as u64,
            layers: frame.layers.as_ptr(),
            layers_len: frame.layers.len() as u64,
            table_attrs: ctx.table_attrs.as_ptr(),
            table_attrs_len: ctx.table_attrs.len() as u64,
            table_entries: ctx.table_entries.as_ptr(),
            table_entries_len: ctx.table_entries.len() as u64,
        };

        let status = unsafe { (self.decode)(self.worker, frame, data, data_cb, &mut err, err_cb) };

        if let Some(err) = err {
            Err(err_msg(err))
        } else if status.done != 0 {
            Ok(Status::Done)
        } else {
            Ok(Status::NextDecoder(DecoderStackID(status.next_decoder)))
        }
    }
}

impl Drop for ParallelWorkerBridge {
    fn drop(&mut self) {
        unsafe { (self.drop)(self.worker) };
    }
}

unsafe extern "C" fn parallel_worker_drop(worker: *mut Box<dyn ParallelWorker>) {
    Box::from_raw(worker);
}

unsafe extern "C" fn parallel_worker_decode(
    worker: *const Box<dyn ParallelWorker>,
    frame: FramePod,
    data: FramePtr,
    data_cb: FrameCallback,
    err: ErrPtr,
    err_cb: ErrCallback,
) -> StatusPod {
    let worker = &*worker;
    let payload = slice::from_raw_parts(frame.payload, frame.payload_len as usize);
    let attrs = slice::from_raw_parts(frame.attrs, frame.attrs_len as usize);
    let layers = slice::from_raw_parts(frame.layers, frame.layers_len as usize);
    let table = AttrTable::new(
        slice::from_raw_parts(frame.table_attrs, frame.table_attrs_len as usize),
        slice::from_raw_parts(frame.table_entries, frame.table_entries_len as usize),
    );
    let mut frame = Frame::new(frame.index, layers, payload, attrs, &table);
    match worker.decode(&mut frame) {
        Err(e) => {
            let s = format!("{}", e);
            err_cb(err, s.as_ptr(), s.len() as u64);
            StatusPod {
                ..Default::default()
            }
        }
        _ => {
            for layer in frame.children().into_iter() {
                let payload = layer.payload();
                let attrs = layer.attrs();
                data_cb(
                    data,
                    FrameCallbackData {
                        payload: payload.as_ptr(),
                        payload_len: payload.len() as u64,
                        attrs: attrs.as_ptr(),
                        attrs_len: attrs.len() as u64,
                    },
                );
            }
            StatusPod {
                done: 1,
                ..Default::default()
            }
        }
    }
}

#[repr(C)]
pub struct SerialWorkerBridge {
    worker: *mut Box<dyn SerialWorker>,
    decode: unsafe extern "C" fn(
        *mut Box<dyn SerialWorker>,
        FramePod,
        ErrPtr,
        ErrCallback,
    ) -> StatusPod,
    drop: unsafe extern "C" fn(*mut Box<dyn SerialWorker>),
}

impl SerialWorkerBridge {
    pub fn new(worker: Box<dyn SerialWorker>) -> Self {
        Self {
            worker: Box::into_raw(Box::new(worker)),
            decode: serial_worker_decode,
            drop: serial_worker_drop,
        }
    }

    pub fn decode(&mut self, frame: &mut FrameData, ctx: &WorkerContext) -> Result<Status> {
        unsafe extern "C" fn err_cb(out: ErrPtr, data: *const u8, len: u64) {
            *out = Some(
                str::from_utf8_unchecked(slice::from_raw_parts(data, len as usize)).to_string(),
            )
        }

        let mut err: Option<String> = None;
        let frame = FramePod {
            index: frame.index(),
            payload: frame.payload.as_ptr(),
            payload_len: frame.payload.len() as u64,
            attrs: frame.attrs.as_ptr(),
            attrs_len: frame.attrs.len() as u64,
            layers: frame.layers.as_ptr(),
            layers_len: frame.layers.len() as u64,
            table_attrs: ctx.table_attrs.as_ptr(),
            table_attrs_len: ctx.table_attrs.len() as u64,
            table_entries: ctx.table_entries.as_ptr(),
            table_entries_len: ctx.table_entries.len() as u64,
        };

        let status = unsafe { (self.decode)(self.worker, frame, &mut err, err_cb) };

        if let Some(err) = err {
            Err(err_msg(err))
        } else if status.done != 0 {
            Ok(Status::Done)
        } else {
            Ok(Status::NextDecoder(DecoderStackID(status.next_decoder)))
        }
    }
}

impl Drop for SerialWorkerBridge {
    fn drop(&mut self) {
        unsafe { (self.drop)(self.worker) };
    }
}

unsafe extern "C" fn serial_worker_drop(worker: *mut Box<dyn SerialWorker>) {
    Box::from_raw(worker);
}

unsafe extern "C" fn serial_worker_decode(
    worker: *mut Box<dyn SerialWorker>,
    frame: FramePod,
    err: ErrPtr,
    err_cb: ErrCallback,
) -> StatusPod {
    let worker = &mut *worker;
    let payload = slice::from_raw_parts(frame.payload, frame.payload_len as usize);
    let attrs = slice::from_raw_parts(frame.attrs, frame.attrs_len as usize);
    let layers = slice::from_raw_parts(frame.layers, frame.layers_len as usize);
    let table = AttrTable::new(
        slice::from_raw_parts(frame.table_attrs, frame.table_attrs_len as usize),
        slice::from_raw_parts(frame.table_entries, frame.table_entries_len as usize),
    );
    let mut frame = Frame::new(frame.index, layers, payload, attrs, &table);
    match worker.decode(&mut frame) {
        Err(e) => {
            let s = format!("{}", e);
            err_cb(err, s.as_ptr(), s.len() as u64);
            StatusPod {
                ..Default::default()
            }
        }
        _ => StatusPod {
            done: 1,
            ..Default::default()
        },
    }
}

#[repr(C)]
#[derive(Default)]
struct StatusPod {
    done: u8,
    next_decoder: u32,
}

#[repr(C)]
struct FramePod {
    index: u64,
    payload: *const u8,
    payload_len: u64,
    attrs: *const Attr,
    attrs_len: u64,
    layers: *const LayerData,
    layers_len: u64,
    table_attrs: *const Attr,
    table_attrs_len: u64,
    table_entries: *const AttrTableEntry,
    table_entries_len: u64,
}

#[cfg(test)]
mod tests {
    use crate::{bridge::DecoderBridge, decoder::Decoder, result::Result, worker::ParallelWorker};
    use failure::err_msg;
    use genet_attr::attr::{AttrType, AttrTypeID, AttrTypeSetID};
    use genet_base::decoder::DecoderStackID;
    use genet_context::Context;
    use genet_token::Token;

    #[test]
    fn decoder_bridge() {
        struct TestDecoder {}

        impl Decoder for TestDecoder {
            fn parallel_worker(&self, _ctx: &mut dyn Context) -> Result<Box<dyn ParallelWorker>> {
                Err(err_msg("test error"))
            }
        }

        struct TestContext {}

        impl Context for TestContext {
            fn get_token(&mut self, _id: &str) -> Token {
                Token::default()
            }

            fn get_decoder(&mut self, _id: &str) -> Result<DecoderStackID> {
                Ok(DecoderStackID(0))
            }

            fn register_attr_type(&mut self, _attr: &AttrType) -> AttrTypeID {
                AttrTypeID(0)
            }

            fn register_attr_type_set(&mut self, _attrs: &[AttrTypeID]) -> AttrTypeSetID {
                AttrTypeSetID(0)
            }
        }

        let bridge = DecoderBridge::new(TestDecoder {});

        assert_eq!(
            bridge
                .parallel_worker(&mut TestContext {})
                .err()
                .unwrap()
                .to_string(),
            "test error".to_string()
        );
    }
}
