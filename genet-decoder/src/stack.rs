use crate::{
    bridge::{DecoderBridge, ParallelWorkerBridge, SerialWorkerBridge, WorkerContext},
    result::Result,
    status::Status,
};
use genet_base::frame::FrameData;
use genet_context::Context;
use std::sync::Arc;

pub struct ParallelStack {
    root: ParallelWorkerBridge,
    children: Vec<ParallelStack>,
}

impl ParallelStack {
    pub fn decode(
        &self,
        frame: &mut FrameData,
        stacks: &[ParallelStack],
        ctx: &WorkerContext,
    ) -> Result<Status> {
        let status = self.root.decode(frame, ctx)?;
        if let Status::NextDecoder(id) = status {
            if let Some(stack) = stacks.get(id.0 as usize) {
                stack.decode(frame, stacks, ctx)?;
            }
        }
        for child in self.children.iter() {
            let _ = child.decode(frame, stacks, ctx);
        }
        Ok(status)
    }
}

pub struct SerialStack {
    root: SerialWorkerBridge,
    children: Vec<SerialStack>,
}

impl SerialStack {
    pub fn decode(
        &mut self,
        frame: &mut FrameData,
        stacks: &mut [Option<SerialStack>],
        ctx: &WorkerContext,
    ) -> Result<Status> {
        let status = self.root.decode(frame, ctx)?;
        if let Status::NextDecoder(id) = status {
            let mut current = None;
            if let Some(stack) = stacks.get_mut(id.0 as usize) {
                std::mem::swap(stack, &mut current);
            }
            if let Some(current) = &mut current {
                current.decode(frame, stacks, ctx)?;
            }
            // TODO
            if let Some(stack) = stacks.get_mut(id.0 as usize) {
                std::mem::swap(stack, &mut current);
            }
        }
        for child in self.children.iter_mut() {
            let _ = child.decode(frame, stacks, ctx);
        }
        Ok(status)
    }
}

#[derive(Clone)]
pub struct StackFactory {
    root: Arc<DecoderBridge>,
    children: Vec<StackFactory>,
}

impl StackFactory {
    pub fn new(root: Arc<DecoderBridge>, children: Vec<StackFactory>) -> Self {
        Self { root, children }
    }

    pub fn parallel(&self, ctx: &mut dyn Context) -> Option<ParallelStack> {
        self.root
            .parallel_worker(ctx)
            .map(|root| ParallelStack {
                root,
                children: self
                    .children
                    .iter()
                    .filter_map(|stack| stack.parallel(ctx))
                    .collect(),
            })
            .ok()
    }

    pub fn serial(&self, ctx: &mut dyn Context) -> Option<SerialStack> {
        self.root
            .serial_worker(ctx)
            .map(|root| SerialStack {
                root,
                children: self
                    .children
                    .iter()
                    .filter_map(|stack| stack.serial(ctx))
                    .collect(),
            })
            .ok()
    }
}
