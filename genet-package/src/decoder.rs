use genet_decoder::bridge::DecoderBridge;
use serde_derive::{Deserialize, Serialize};
use std::sync::Arc;

#[derive(Default, Deserialize, Serialize)]
pub struct DecoderComponent {
    pub id: String,
    pub trigger_after: Vec<String>,

    #[serde(skip)]
    pub decoder: Option<Arc<DecoderBridge>>,
}
