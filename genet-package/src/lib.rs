#![forbid(unsafe_code)]

use serde_derive::{Deserialize, Serialize};
pub mod decoder;

#[derive(Deserialize, Serialize)]
pub enum Component {
    Reader,
    Weiter,
    Decoder(decoder::DecoderComponent),
}

#[derive(Deserialize, Serialize)]
pub struct Package {
    pub id: String,
    pub name: String,
    pub description: String,
    pub components: Vec<Component>,
}
