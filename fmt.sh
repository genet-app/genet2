#!/bin/sh

find . -name Cargo.toml|while read fname; do
  DIR=$(dirname "$fname")
  (cd $DIR; cargo +nightly fmt; cargo fix --allow-dirty; cargo tomlfmt)
done
