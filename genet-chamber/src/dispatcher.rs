use crate::context::{ChamberContext, DecoderContext};
use genet_base::frame::FrameData;
use genet_decoder::{stack::SerialStack, status::Status};
use rayon::{self, iter::ParallelIterator, prelude::*};
use std::thread::JoinHandle;

#[derive(Clone)]
pub struct ParallelDispatcher {
    ctx: DecoderContext,
}

impl ParallelDispatcher {
    pub fn new(ctx: &ChamberContext) -> Self {
        Self {
            ctx: ctx.decoder_context(),
        }
    }

    pub fn dispatch(&self, list: Vec<FrameData>) -> Vec<FrameData> {
        list.into_par_iter()
            .map_with(self.clone(), |disp, mut frame| {
                match disp.ctx.decode(&mut frame) {
                    Ok(Status::Done) => (),
                    _ => (),
                }
                frame
            })
            .collect()
    }
}

pub struct SerialDispatcher {
    ctx: DecoderContext,
    stack: Vec<SerialStack>,
}

impl SerialDispatcher {
    pub fn new(ctx: &ChamberContext) -> Self {
        Self {
            ctx: ctx.decoder_context(),
            stack: Vec::new(),
        }
    }

    pub fn dispatch(&mut self, list: Vec<FrameData>, req: Vec<Request>) -> Vec<FrameData> {
        let mut list = list;
        let mut req = req;
        while let Some(req) = req.pop() {
            let _frame = &mut list[req.offset];
            // let _ = self.stack[req.decoder].decode(frame, &self.ctx.worker_ctx);
        }
        list
    }
}

pub struct Request {
    decoder: usize,
    offset: usize,
}
