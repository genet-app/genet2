use crate::{profile::Profile, result::Result};
use failure::format_err;
use genet_attr::attr::{Attr, AttrTableEntry, AttrType, AttrTypeID, AttrTypeSetID};
use genet_base::{decoder::DecoderStackID, frame::FrameData, layer::LayerTypeID};
use genet_context::Context;
use genet_decoder::{bridge::WorkerContext, stack::StackFactory, status::Status};
use genet_package::Component;
use genet_token::Token;
use parking_lot::Mutex;
use std::{
    cell::RefCell,
    collections::{HashMap, HashSet},
    sync::Arc,
};

#[derive(Clone)]
pub struct ChamberContext {
    inner: Arc<Mutex<Inner>>,
}

impl ChamberContext {
    pub fn new(profile: Profile) -> Self {
        Self {
            inner: Arc::new(Mutex::new(Inner::new(profile))),
        }
    }

    pub(crate) fn decoder_context(&self) -> DecoderContext {
        let inner = self.inner.lock();
        DecoderContext {
            attrs: inner.attrs.clone(),
            worker_ctx: inner.worker_ctx.clone(),
        }
    }
}

impl Context for ChamberContext {
    fn get_token(&mut self, id: &str) -> Token {
        let inner = self.inner.lock();
        inner.get_token(id)
    }

    fn get_decoder(&mut self, id: &str) -> Result<DecoderStackID> {
        let mut inner = self.inner.lock();

        let token = inner.get_token(id);
        if let Some(decoder) = inner.decoder_indices.get(&token) {
            return (*decoder).ok_or_else(|| format_err!("no such decoder: {}", id));
        }

        let mut used = HashSet::new();
        let decoder = if let Some(factory) = inner.stack_factory(id, &mut used) {
            let id = DecoderStackID(inner.decoders.len() as u32);
            inner.decoders.push(factory);
            Some(id)
        } else {
            None
        };

        (*inner.decoder_indices.entry(token).or_insert(decoder))
            .ok_or_else(|| format_err!("no such decoder: {}", id))
    }

    fn register_attr_type(&mut self, attr: &AttrType) -> AttrTypeID {
        let mut inner = self.inner.lock();
        let id = inner.attrs.len() as u32;
        inner.attrs.push(attr.clone());
        AttrTypeID(id)
    }

    fn register_attr_type_set(&mut self, attrs: &[AttrTypeID]) -> AttrTypeSetID {
        let mut inner = self.inner.lock();
        let id = inner.worker_ctx.table_entries.len() as u32;
        let mut entry = AttrTableEntry {
            start: inner.worker_ctx.table_attrs.len() as u64,
            end: 0,
        };
        let mut attrs = attrs.iter().map(|id| inner.to_attr(*id)).collect();
        inner.worker_ctx.table_attrs.append(&mut attrs);
        entry.end = inner.worker_ctx.table_attrs.len() as u64;
        inner.worker_ctx.table_entries.push(entry);
        AttrTypeSetID(id)
    }
}

struct Inner {
    profile: Profile,
    tokens: RefCell<HashMap<String, Token>>,
    attrs: Vec<AttrType>,
    worker_ctx: WorkerContext,
    decoders: Vec<StackFactory>,
    decoder_indices: HashMap<Token, Option<DecoderStackID>>,
}

impl Inner {
    fn new(profile: Profile) -> Self {
        Self {
            profile,
            tokens: RefCell::new(HashMap::new()),
            attrs: Vec::new(),
            worker_ctx: Default::default(),
            decoders: Vec::new(),
            decoder_indices: HashMap::new(),
        }
    }

    fn get_token(&self, id: &str) -> Token {
        if id.is_empty() {
            return Token::default();
        }
        let index = self.tokens.borrow().len() as u32 + 1;
        *self
            .tokens
            .borrow_mut()
            .entry(id.into())
            .or_insert_with(|| Token::new(index, id))
    }

    fn stack_factory(&self, id: &str, used: &mut HashSet<String>) -> Option<StackFactory> {
        if used.contains(id) {
            return None;
        }
        used.insert(id.to_string());

        let components = self.profile.components();
        let children = components
            .iter()
            .filter_map(|comp| match comp {
                Component::Decoder(decoder) => decoder
                    .trigger_after
                    .iter()
                    .find(|&x| x == id)
                    .map(|_| &decoder.id),
                _ => None,
            })
            .filter_map(|child| self.stack_factory(child, used))
            .collect();

        components
            .iter()
            .filter_map(|comp| match comp {
                Component::Decoder(decoder) if decoder.id == id => Some(decoder),
                _ => None,
            })
            .next()
            .and_then(|d| d.decoder.clone())
            .map(|d| StackFactory::new(d, children))
    }

    fn to_attr(&self, ty: AttrTypeID) -> Attr {
        self.attrs
            .get(ty.0 as usize)
            .map(|attr| Attr {
                ty,
                id: self.get_token(&attr.id),
                kind: self.get_token(&attr.kind),
                alias: self.get_token(&attr.alias),
                bit_range_start: attr.bit_range.start as u64,
                bit_range_end: attr.bit_range.end as u64,
            })
            .unwrap_or_default()
    }
}

#[derive(Clone)]
pub struct DecoderContext {
    pub attrs: Vec<AttrType>,
    pub worker_ctx: WorkerContext,
}

impl DecoderContext {
    pub fn decode(&self, _frame: &mut FrameData) -> Result<Status> {
        Ok(Status::Done)
    }
}
