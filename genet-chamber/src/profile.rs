use genet_package::Component;

pub struct Profile {
    components: Vec<Component>,
}

impl Profile {
    pub fn new() -> Self {
        Self {
            components: Vec::new(),
        }
    }

    pub fn components(&self) -> &[Component] {
        &self.components
    }
}
