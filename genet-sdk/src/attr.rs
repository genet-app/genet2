use crate::field::{Field, FieldContext};
pub use genet_attr::attr::{Attr, AttrType};

pub struct Node<T: Field> {
    field: T,
}

impl<T: Field> Node<T> {
    pub fn new(_ctx: FieldContext) {}
}
