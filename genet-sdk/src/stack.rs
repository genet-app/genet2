use genet_context::Context;

pub struct DecoderStack {}

impl DecoderStack {
    pub fn new(_ctx: &mut dyn Context, _id: &str) -> Self {
        Self {}
    }
}
