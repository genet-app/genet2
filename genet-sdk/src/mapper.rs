use genet_attr::value::OwnedValue;
use std::fmt;

pub trait Mapper: Send + Sync {
    fn map(&self, value: OwnedValue, data: &[u8]) -> OwnedValue;
    fn box_clone(&self) -> Box<dyn Mapper>;
}

impl fmt::Debug for dyn Mapper {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Mapper")
    }
}

impl Clone for Box<dyn Mapper> {
    fn clone(&self) -> Box<dyn Mapper> {
        self.box_clone()
    }
}

pub struct MapperFunc<F>(F);

impl<F: 'static + Fn(OwnedValue, &[u8]) -> OwnedValue + Send + Sync + Clone> MapperFunc<F> {
    pub fn wrap(func: F) -> Box<dyn Mapper> {
        Box::new(MapperFunc(func))
    }
}

impl<F: 'static + Fn(OwnedValue, &[u8]) -> OwnedValue + Send + Sync + Clone> Mapper
    for MapperFunc<F>
{
    fn map(&self, value: OwnedValue, data: &[u8]) -> OwnedValue {
        (self.0)(value, data)
    }

    fn box_clone(&self) -> Box<dyn Mapper> {
        Box::new(MapperFunc(self.0.clone()))
    }
}
