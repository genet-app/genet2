use crate::{
    attr::Attr,
    context::Context,
    field::{Field, FieldContext, FieldTable, FieldTableEntry},
    result::Result,
    value::{IntoOwnedValue, OwnedValue},
};
use failure::err_msg;
use genet_attr::attr::AttrTypeSetID;
pub use genet_base::layer::Layer;
use std::ops::{Deref, Range};

pub struct LayerType<T> {
    attr: AttrTypeSetID,
    field: T,
    table: FieldTable,
}

impl<T> Deref for LayerType<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.field
    }
}

impl<T: Field> LayerType<T> {
    pub fn new(ctx: &mut dyn Context, id: &str, field: T) -> Self {
        let field_ctx = FieldContext {
            id: id.into(),
            ..Default::default()
        };
        let mut table: FieldTable = Default::default();
        for field in T::field_attrs(ctx, &field_ctx) {
            let index = field.index;
            let attr = Attr {
                id: ctx.get_token(&field.id),
                ty: ctx.register_attr_type(&field),
                alias: ctx.get_token(&field.alias),
                kind: ctx.get_token(&field.kind),
                bit_range_start: field.bit_range.start as u64,
                bit_range_end: field.bit_range.end as u64,
            };
            let entry = FieldTableEntry {
                parser: field.parser,
                attr,
            };
            if let Some(index) = index {
                table.push_indexed(index, entry);
            } else {
                table.push(entry);
            }
        }
        let attrs = table.entries().collect::<Vec<_>>();
        println!("{:#?}", attrs);
        let attrs = table
            .entries()
            .map(|entry| entry.attr.ty)
            .collect::<Vec<_>>();
        Self {
            attr: ctx.register_attr_type_set(&attrs),
            field,
            table,
        }
    }

    fn get_range_opt<F: Field, O>(
        &self,
        layer: &Layer,
        field: &F,
        byte_range: Option<Range<usize>>,
    ) -> Result<O>
    where
        O: From<F::Output>,
        F::Output: Sized + From<OwnedValue>,
    {
        let offset = field as *const F as usize - &self.field as *const T as usize;
        let index = F::field_index(offset);
        self.table
            .get(index)
            .ok_or_else(|| err_msg("wrong field"))
            .and_then(|entry| {
                let bit_range = byte_range
                    .map(|range| range.start * 8..range.end * 8)
                    .unwrap_or(
                        entry.attr.bit_range_start as usize..entry.attr.bit_range_end as usize,
                    );
                let attr = Attr {
                    bit_range_start: bit_range.start as u64,
                    bit_range_end: bit_range.end as u64,
                    ..entry.attr.clone()
                };
                entry.parser.parse(&attr, layer.payload())
            })
            .map(|value| Into::<F::Output>::into(value.into_owned_value(layer.payload())).into())
    }

    pub fn get<F: Field, O>(&self, layer: &Layer, field: &F) -> Result<O>
    where
        O: From<F::Output>,
        F::Output: Sized + From<OwnedValue>,
    {
        self.get_range_opt(layer, field, None)
    }

    pub fn get_range<F: Field, O>(
        &self,
        layer: &Layer,
        field: &F,
        byte_range: Range<usize>,
    ) -> Result<O>
    where
        O: From<F::Output>,
        F::Output: Sized + From<OwnedValue>,
    {
        self.get_range_opt(layer, field, Some(byte_range))
    }
}
