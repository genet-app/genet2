use crate::{
    context::Context,
    mapper::{Mapper, MapperFunc},
    result::Result,
};
use byteorder::{BigEndian, LittleEndian, ReadBytesExt};
use failure::err_msg;
use genet_attr::value::{IntoOwnedValue, Value};
pub use genet_attr::{
    attr::{Attr, AttrType},
    parser::{Parser, ParserBridge, ParserFunc},
};
use std::{borrow::Cow, io::Cursor, mem::size_of, ops::Range};

#[derive(Clone, Debug)]
pub struct FieldContext {
    pub offset: usize,
    pub id: String,
    pub alias: String,
    pub kind: String,
    pub bit_range: Range<usize>,
    pub name: String,
    pub description: String,
    pub little_endian: bool,
    pub mapper: Box<dyn Mapper>,
    pub parser: Box<dyn Parser>,
}

impl Default for FieldContext {
    fn default() -> Self {
        Self {
            offset: Default::default(),
            id: Default::default(),
            alias: Default::default(),
            kind: Default::default(),
            bit_range: 0..0,
            name: Default::default(),
            description: Default::default(),
            little_endian: Default::default(),
            mapper: MapperFunc::wrap(|v, _| v),
            parser: ParserFunc::wrap(|_, _| Err(err_msg("not yet implemented"))),
        }
    }
}

#[derive(Debug, Default)]
pub struct FieldTableEntry {
    pub parser: ParserBridge,
    pub attr: Attr,
}

#[derive(Debug, Default)]
pub struct FieldTable {
    pub indices: Vec<i16>,
    pub indexed_entries: Vec<FieldTableEntry>,
    pub entries: Vec<FieldTableEntry>,
}

impl FieldTable {
    pub fn push(&mut self, entry: FieldTableEntry) {
        self.entries.push(entry);
    }

    pub fn push_indexed(&mut self, index: usize, entry: FieldTableEntry) {
        if self.indices.len() <= index {
            self.indices.resize(index + 1, -1);
        }
        self.indices[index] = self.indexed_entries.len() as i16;
        self.indexed_entries.push(entry);
    }

    pub fn get(&self, index: usize) -> Option<&FieldTableEntry> {
        self.indices
            .get(index)
            .and_then(|&i| self.indexed_entries.get(i as usize))
    }

    pub fn entries(&self) -> impl Iterator<Item = &FieldTableEntry> {
        self.indexed_entries.iter().chain(self.entries.iter())
    }
}

pub trait FieldType {
    fn parser(field: &FieldContext) -> Box<dyn Parser>;
}

pub trait Field {
    type Output: ?Sized;
    fn field_index(offset: usize) -> usize;
    fn field_attrs(_ctx: &mut dyn Context, _parent_field: &FieldContext) -> Vec<AttrType>;
    fn bit_size() -> usize;
}

pub trait EnumField<T> {
    fn attrs<Parent: Field>(ctx: &mut dyn Context, field: &FieldContext) -> Vec<Attr>;
}

macro_rules! define_field {
    ($t:ty, $size:expr, $little:block, $big:block) => {
        impl FieldType for $t {
            fn parser(field: &FieldContext) -> Box<dyn Parser> {
                let field = field.clone();
                ParserFunc::wrap(move |attr, data| {
                    data.get(attr.byte_range())
                        .ok_or_else(|| err_msg("out of bounds"))
                        .and_then(|data| {
                            let parse: fn(&[u8], Range<usize>) -> Result<Self> =
                                if field.little_endian { $little } else { $big };
                            parse(data, attr.bit_range())
                        })
                        .map(|v| {
                            field
                                .mapper
                                .map(Into::<Value>::into(v).into_owned_value(data), data)
                        })
                })
            }
        }

        impl Field for $t {
            type Output = Self;

            fn field_attrs(_ctx: &mut dyn Context, _parent_field: &FieldContext) -> Vec<AttrType> {
                vec![]
            }

            fn field_index(offset: usize) -> usize {
                offset * 2
            }

            fn bit_size() -> usize {
                $size
            }
        }
    };
}

define_field!(
    u8,
    size_of::<Self>() * 8,
    { |data, _range| Cursor::new(data).read_u8().map_err(|e| e.into()) },
    { |data, _range| Cursor::new(data).read_u8().map_err(|e| e.into()) }
);

define_field!(
    u16,
    size_of::<Self>() * 8,
    {
        |data, _range| {
            Cursor::new(data)
                .read_u16::<LittleEndian>()
                .map_err(|e| e.into())
        }
    },
    {
        |data, _range| {
            Cursor::new(data)
                .read_u16::<BigEndian>()
                .map_err(|e| e.into())
        }
    }
);

define_field!(
    u32,
    size_of::<Self>() * 8,
    {
        |data, _range| {
            Cursor::new(data)
                .read_u32::<LittleEndian>()
                .map_err(|e| e.into())
        }
    },
    {
        |data, _range| {
            Cursor::new(data)
                .read_u32::<BigEndian>()
                .map_err(|e| e.into())
        }
    }
);

define_field!(
    u64,
    size_of::<Self>() * 8,
    {
        |data, _range| {
            Cursor::new(data)
                .read_u64::<LittleEndian>()
                .map_err(|e| e.into())
        }
    },
    {
        |data, _range| {
            Cursor::new(data)
                .read_u64::<BigEndian>()
                .map_err(|e| e.into())
        }
    }
);

define_field!(
    i8,
    size_of::<Self>() * 8,
    { |data, _range| Cursor::new(data).read_i8().map_err(|e| e.into()) },
    { |data, _range| Cursor::new(data).read_i8().map_err(|e| e.into()) }
);

define_field!(
    i16,
    size_of::<Self>() * 8,
    {
        |data, _range| {
            Cursor::new(data)
                .read_i16::<LittleEndian>()
                .map_err(|e| e.into())
        }
    },
    {
        |data, _range| {
            Cursor::new(data)
                .read_i16::<BigEndian>()
                .map_err(|e| e.into())
        }
    }
);

define_field!(
    i32,
    size_of::<Self>() * 8,
    {
        |data, _range| {
            Cursor::new(data)
                .read_i32::<LittleEndian>()
                .map_err(|e| e.into())
        }
    },
    {
        |data, _range| {
            Cursor::new(data)
                .read_i32::<BigEndian>()
                .map_err(|e| e.into())
        }
    }
);

define_field!(
    i64,
    size_of::<Self>() * 8,
    {
        |data, _range| {
            Cursor::new(data)
                .read_i64::<LittleEndian>()
                .map_err(|e| e.into())
        }
    },
    {
        |data, _range| {
            Cursor::new(data)
                .read_i64::<BigEndian>()
                .map_err(|e| e.into())
        }
    }
);

impl Field for Vec<u8> {
    type Output = [u8];

    fn field_attrs(_ctx: &mut dyn Context, _parent_field: &FieldContext) -> Vec<AttrType> {
        vec![]
    }

    fn field_index(offset: usize) -> usize {
        offset * 2
    }

    fn bit_size() -> usize {
        8
    }
}

impl FieldType for [u8] {
    fn parser(field: &FieldContext) -> Box<dyn Parser> {
        let field = field.clone();
        ParserFunc::wrap(move |attr, data| {
            data.get(attr.byte_range())
                .ok_or_else(|| err_msg("out of bounds"))
                .map(|v| {
                    field
                        .mapper
                        .map(Value::Bytes(Cow::Borrowed(v)).into_owned_value(data), data)
                })
        })
    }
}

impl Field for [u8] {
    type Output = [u8];

    fn field_attrs(_ctx: &mut dyn Context, _parent_field: &FieldContext) -> Vec<AttrType> {
        vec![]
    }

    fn field_index(offset: usize) -> usize {
        offset * 2
    }

    fn bit_size() -> usize {
        8
    }
}

/*
impl<N: Field, C> Node<N, C>
where
    N::Output: From<OwnedValue>,
{
    pub fn get_range(&self, layer: &Layer, byte_range: Range<usize>) -> Result<N::Output> {
        let result = self
            .parser
            .parse(&attr_with_range(&self.attr, byte_range), layer.payload());
        result.map(|v| v.into())
    }

    pub fn get(&self, layer: &Layer) -> Result<N::Output> {
        self.get_range(layer, self.attr.byte_range())
    }
}

impl<N: Field, C> Node<N, C> {
    pub fn get_slice_range<'a>(
        &self,
        layer: &'a Layer,
        byte_range: Range<usize>,
    ) -> Result<&'a [u8]> {
        let result = self
            .parser
            .parse(&attr_with_range(&self.attr, byte_range), layer.payload());
        result.and_then(|v| match v {
            OwnedValue::Bytes(start, end) => Ok(&layer.payload()[start..end]),
            _ => Err(err_msg("type mismatch")),
        })
    }

    pub fn get_slice<'a>(&self, layer: &'a Layer) -> Result<&'a [u8]> {
        self.get_slice_range(layer, self.attr.byte_range())
    }
}

impl<N: Field, E: EnumField<N::Output>> Field for Enum<N, E>
where
    N::Output: Sized,
{
    type Output = N::Output;

    fn parser(field: &FieldContext) -> Box<dyn Parser> {
        N::parser(field)
    }
}

impl<N: Field, E: EnumField<N::Output>> Enum<N, E>
where
    N::Output: Sized + From<OwnedValue>,
{
    pub fn get_range(&self, layer: &Layer, byte_range: Range<usize>) -> Result<N::Output> {
        let result = self
            .parser
            .parse(&attr_with_range(&self.attr, byte_range), layer.payload());
        result.map(|v| v.into())
    }

    pub fn get(&self, layer: &Layer) -> Result<N::Output> {
        self.get_range(layer, self.attr.byte_range())
    }
}
*/
