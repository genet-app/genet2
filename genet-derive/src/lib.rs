#![recursion_limit = "256"]
#![forbid(unsafe_code)]

extern crate proc_macro;
use proc_macro::TokenStream;

mod attr;
mod initialisms;
mod package;

#[proc_macro_derive(Package, attributes(decoder, reader, writer))]
pub fn derive_package(input: TokenStream) -> TokenStream {
    package::derive_package(input)
}

#[proc_macro_derive(Attr, attributes(attr))]
pub fn derive_attr(input: TokenStream) -> TokenStream {
    attr::derive_attr(input)
}
