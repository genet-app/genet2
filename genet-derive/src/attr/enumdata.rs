use crate::initialisms::to_title_case;
use inflector::cases::camelcase::to_camel_case;
use proc_macro2::TokenStream;
use quote::*;
use std::str::FromStr;
use syn::{DataEnum, DeriveInput, Ident, Lit, Meta, MetaNameValue, NestedMeta, Variant};

#[derive(Debug, Default)]
struct EnumMetadata {
    pub ty: Option<String>,
}

impl EnumMetadata {
    pub fn new(input: &DeriveInput) -> Self {
        let attrs = &input.attrs;
        attrs
            .iter()
            .filter_map(|attr| attr.interpret_meta())
            .fold(Default::default(), |data, meta| {
                parse_enum_meta(&meta, data)
            })
    }
}

fn parse_enum_meta(meta: &Meta, data: EnumMetadata) -> EnumMetadata {
    let name = meta.name().to_string();
    match (name.as_str(), meta) {
        ("attr", meta) => parse_enum_attr(meta, data),
        ("doc", _) => data,
        _ => panic!("unsupported attribute: {}", name),
    }
}

fn parse_enum_attr(meta: &Meta, data: EnumMetadata) -> EnumMetadata {
    if let Meta::List(list) = meta {
        list.nested
            .iter()
            .filter_map(|item| {
                if let NestedMeta::Meta(meta) = item {
                    Some(meta)
                } else {
                    None
                }
            })
            .fold(data, |data, meta| parse_enum_attr_meta(meta, data))
    } else {
        panic!("attr attribute must be a list")
    }
}

fn parse_enum_attr_meta(meta: &Meta, data: EnumMetadata) -> EnumMetadata {
    let name = meta.name().to_string();
    match (name.as_str(), meta) {
        (
            name,
            Meta::NameValue(MetaNameValue {
                lit: Lit::Str(lit_str),
                ..
            }),
        ) => match name {
            "ty" => EnumMetadata {
                ty: Some(lit_str.value().trim().into()),
                ..data
            },
            _ => panic!("unsupported attribute: {}", name),
        },
        _ => panic!("unsupported attribute: {}", name),
    }
}

#[derive(Debug, Default)]
struct AttrMetadata {
    pub id: String,
    pub name: String,
    pub description: String,
    pub docs: String,
}

impl AttrMetadata {
    pub fn new(var: &Variant) -> Self {
        let attrs = &var.attrs;
        let data = attrs
            .iter()
            .filter_map(|attr| attr.interpret_meta())
            .fold(Default::default(), |data, meta| parse_meta(&meta, data));
        parse_field(var, data)
    }
}

fn parse_field(var: &Variant, data: AttrMetadata) -> AttrMetadata {
    let mut lines = data.docs.split('\n').map(|line| line.trim());
    let ident = var.ident.to_string();
    let name = lines
        .next()
        .map(|line| line.trim().to_string())
        .filter(|name| !name.is_empty())
        .unwrap_or_else(|| to_title_case(&ident));
    let description = lines
        .fold(String::new(), |acc, x| acc + x + "\n")
        .trim()
        .to_string();
    AttrMetadata {
        id: to_camel_case(
            &Some(data.id)
                .filter(|id| !id.is_empty())
                .unwrap_or_else(|| ident.to_string()),
        ),
        name,
        description,
        ..data
    }
}

fn parse_meta(meta: &Meta, data: AttrMetadata) -> AttrMetadata {
    let name = meta.name().to_string();
    match (name.as_str(), meta) {
        ("doc", meta) => parse_doc(meta, data),
        ("attr", meta) => parse_attr(meta, data),
        _ => panic!("unsupported attribute: {}", name),
    }
}

fn parse_attr(meta: &Meta, data: AttrMetadata) -> AttrMetadata {
    if let Meta::List(list) = meta {
        list.nested
            .iter()
            .filter_map(|item| {
                if let NestedMeta::Meta(meta) = item {
                    Some(meta)
                } else {
                    None
                }
            })
            .fold(data, |data, meta| parse_attr_meta(meta, data))
    } else {
        panic!("attr attribute must be a list")
    }
}

fn parse_attr_meta(meta: &Meta, data: AttrMetadata) -> AttrMetadata {
    let name = meta.name().to_string();
    match (name.as_str(), meta) {
        (
            name,
            Meta::NameValue(MetaNameValue {
                lit: Lit::Str(lit_str),
                ..
            }),
        ) => match name {
            "id" => AttrMetadata {
                id: lit_str.value().trim().into(),
                ..data
            },
            _ => panic!("unsupported attribute: {}", name),
        },
        _ => panic!("unsupported attribute: {}", name),
    }
}

fn parse_doc(meta: &Meta, data: AttrMetadata) -> AttrMetadata {
    let mut data = data;
    if let Meta::NameValue(MetaNameValue {
        lit: Lit::Str(lit_str),
        ..
    }) = meta
    {
        data.docs += &lit_str.value();
        data.docs += "\n";
    }
    data
}

fn derive_field_attr(ident: &Ident, field: &Variant, attr: &AttrMetadata) -> TokenStream {
    let id = &attr.id;
    let name = &attr.name;
    let description = &attr.description;
    let field_ident = &field.ident;
    quote! {
        {
            use genet_sdk::value::OwnedValue;
            let parent_parser = parent_field.parser.clone();
            let attr = genet_sdk::field::AttrType {
                index: None,
                id: format!("{}.{}", parent_field.id, #id).trim_matches('.').into(),
                bit_range: parent_field.bit_range.clone(),
                name: #name.into(),
                description: #description.into(),
                parser: genet_sdk::field::ParserBridge::new(genet_sdk::field::ParserFunc::wrap(move |attr, data| {
                    parent_parser.parse(attr, data).map(|value| {
                        let value : Self::Output = value.into();
                        let value : Self = value.into();
                        match value {
                            #ident::#field_ident => OwnedValue::Bool(true),
                            _ => OwnedValue::Nil
                        }
                    })
                })),
                ..Default::default()
            };

            indices.push(attr);
        }
    }
}

fn derive_field_attrs(
    ident: &Ident,
    fields: &[&Variant],
    attrs: &[AttrMetadata],
) -> Vec<TokenStream> {
    fields
        .iter()
        .zip(attrs.iter())
        .map(|(field, attr)| derive_field_attr(ident, field, attr))
        .collect()
}

pub(crate) fn derive_attrs(input: &DeriveInput, data: &DataEnum) -> TokenStream {
    let ident = &input.ident;
    let enum_attr = EnumMetadata::new(input);
    let (impl_generics, ty_generics, _) = input.generics.split_for_impl();

    let fields = data.variants.iter().collect::<Vec<_>>();
    let attrs = fields
        .iter()
        .map(|&field| AttrMetadata::new(field))
        .collect::<Vec<_>>();

    let field_attrs = derive_field_attrs(ident, &fields, &attrs);

    let output_ty = enum_attr
        .ty
        .as_ref()
        .map(|ty| TokenStream::from_str(ty).unwrap())
        .expect("ty attribute is required for Enum");

    let tokens = quote! {
        impl #impl_generics genet_sdk::field::Field for #ident #ty_generics where #output_ty: Into<#ident> {
            type Output = #output_ty;

            fn field_attrs(ctx: &mut dyn genet_sdk::context::Context, parent_field: &genet_sdk::field::FieldContext) -> Vec<genet_sdk::field::AttrType> {
                let mut indices = vec![];
                #( #field_attrs )*
                indices
            }

            fn field_index(offset: usize) -> usize {
                offset * 2
            }

            fn bit_size() -> usize {
                Self::Output::bit_size()
            }
        }
    };
    tokens.into()
}
