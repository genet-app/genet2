use proc_macro::TokenStream;
use syn::{parse_macro_input, Data, DeriveInput};

mod enumdata;
mod structdata;

pub(crate) fn derive_attr(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let mut tokens = TokenStream::new();
    tokens.extend(impl_parse_bytes(&input));
    tokens
}

fn impl_parse_bytes(input: &DeriveInput) -> TokenStream {
    match &input.data {
        Data::Struct(s) => structdata::derive_attrs(input, &s),
        Data::Enum(s) => enumdata::derive_attrs(input, &s),
        _ => panic!("Attr derive supports struct and enum types only"),
    }
    .into()
}
