use crate::initialisms::to_title_case;
use inflector::cases::camelcase::to_camel_case;
use proc_macro2::{TokenStream, TokenTree};
use quote::*;
use std::str::FromStr;
use syn::{
    DataStruct, DeriveInput, Field, Fields, ImplGenerics, Lit, Meta, MetaNameValue, NestedMeta,
};

#[derive(Debug, Default)]
struct StructMetadata {
    pub ty: Option<String>,
}

impl StructMetadata {
    pub fn new(input: &DeriveInput) -> Self {
        let attrs = &input.attrs;
        attrs
            .iter()
            .filter_map(|attr| attr.interpret_meta())
            .fold(Default::default(), |data, meta| {
                parse_struct_meta(&meta, data)
            })
    }
}

fn parse_struct_meta(meta: &Meta, data: StructMetadata) -> StructMetadata {
    let name = meta.name().to_string();
    match (name.as_str(), meta) {
        ("attr", meta) => parse_struct_attr(meta, data),
        ("doc", _) => data,
        _ => panic!("unsupported attribute: {}", name),
    }
}

fn parse_struct_attr(meta: &Meta, data: StructMetadata) -> StructMetadata {
    if let Meta::List(list) = meta {
        list.nested
            .iter()
            .filter_map(|item| {
                if let NestedMeta::Meta(meta) = item {
                    Some(meta)
                } else {
                    None
                }
            })
            .fold(data, |data, meta| parse_struct_attr_meta(meta, data))
    } else {
        panic!("attr attribute must be a list")
    }
}

fn parse_struct_attr_meta(meta: &Meta, data: StructMetadata) -> StructMetadata {
    let name = meta.name().to_string();
    match (name.as_str(), meta) {
        (
            name,
            Meta::NameValue(MetaNameValue {
                lit: Lit::Str(lit_str),
                ..
            }),
        ) => match name {
            "ty" => StructMetadata {
                ty: Some(lit_str.value().trim().into()),
                ..data
            },
            _ => panic!("unsupported attribute: {}", name),
        },
        _ => panic!("unsupported attribute: {}", name),
    }
}

#[derive(Debug, Default)]
struct FieldMetadata {
    pub id: String,
    pub kind: String,
    pub name: String,
    pub description: String,
    pub alias: String,
    pub input_ty: Option<String>,
    pub bit_size: Option<usize>,
    pub little_endian: bool,
    pub align_before: bool,
    pub var_ident: Option<String>,
    pub func_decode: Option<String>,
    pub func_encode: Option<String>,
    pub skip: bool,
    pub detach: bool,
    pub docs: String,
}

impl FieldMetadata {
    pub fn new(field: &Field) -> Self {
        let attrs = &field.attrs;
        let data = attrs
            .iter()
            .filter_map(|attr| attr.interpret_meta())
            .fold(Default::default(), |data, meta| {
                parse_field_meta(&meta, data)
            });
        parse_field(field, data)
    }
}

fn parse_field(field: &Field, data: FieldMetadata) -> FieldMetadata {
    let mut lines = data.docs.split('\n').map(|line| line.trim());
    let ident = field
        .ident
        .as_ref()
        .map(|ident| ident.to_string())
        .unwrap_or_else(|| String::new());
    let name = lines
        .next()
        .map(|line| line.trim().to_string())
        .filter(|name| !name.is_empty())
        .unwrap_or_else(|| to_title_case(&ident));
    let description = lines
        .fold(String::new(), |acc, x| acc + x + "\n")
        .trim()
        .to_string();
    FieldMetadata {
        id: to_camel_case(
            &Some(data.id)
                .filter(|id| !id.is_empty())
                .unwrap_or_else(|| ident.to_string()),
        ),
        name,
        description,
        ..data
    }
}

fn parse_field_meta(meta: &Meta, data: FieldMetadata) -> FieldMetadata {
    let name = meta.name().to_string();
    match (name.as_str(), meta) {
        ("doc", meta) => parse_doc(meta, data),
        ("attr", meta) => parse_field_attr(meta, data),
        _ => panic!("unsupported attribute: {}", name),
    }
}

fn parse_field_attr(meta: &Meta, data: FieldMetadata) -> FieldMetadata {
    if let Meta::List(list) = meta {
        list.nested
            .iter()
            .filter_map(|item| {
                if let NestedMeta::Meta(meta) = item {
                    Some(meta)
                } else {
                    None
                }
            })
            .fold(data, |data, meta| parse_field_attr_meta(meta, data))
    } else {
        panic!("attr attribute must be a list")
    }
}

fn parse_field_attr_meta(meta: &Meta, data: FieldMetadata) -> FieldMetadata {
    let name = meta.name().to_string();
    match (name.as_str(), meta) {
        ("skip", _) => FieldMetadata { skip: true, ..data },
        ("detach", _) => FieldMetadata {
            detach: true,
            ..data
        },
        ("align_before", _) => FieldMetadata {
            align_before: true,
            ..data
        },
        ("little_endian", _) => FieldMetadata {
            little_endian: true,
            ..data
        },
        ("big_endian", _) => FieldMetadata {
            little_endian: false,
            ..data
        },
        ("native_endian", _) => {
            #[cfg(target_endian = "little")]
            const LITTLE: bool = true;
            #[cfg(target_endian = "big")]
            const LITTLE: bool = false;
            FieldMetadata {
                little_endian: LITTLE,
                ..data
            }
        }
        (
            name,
            Meta::NameValue(MetaNameValue {
                lit: Lit::Str(lit_str),
                ..
            }),
        ) => match name {
            "id" => FieldMetadata {
                id: lit_str.value().trim().into(),
                ..data
            },
            "kind" => FieldMetadata {
                kind: lit_str.value().trim().into(),
                ..data
            },
            "alias" => FieldMetadata {
                alias: lit_str.value().trim().into(),
                ..data
            },
            "var_ident" => FieldMetadata {
                var_ident: Some(lit_str.value().trim().into()),
                ..data
            },
            "ty" => FieldMetadata {
                input_ty: Some(lit_str.value().trim().into()),
                ..data
            },
            "decode" => FieldMetadata {
                func_decode: Some(lit_str.value().trim().into()),
                ..data
            },
            "encode" => FieldMetadata {
                func_encode: Some(lit_str.value().trim().into()),
                ..data
            },
            _ => panic!("unsupported attribute: {}", name),
        },
        (
            name,
            Meta::NameValue(MetaNameValue {
                lit: Lit::Int(lit_int),
                ..
            }),
        ) => match name {
            "byte_size" => FieldMetadata {
                bit_size: Some(lit_int.value() as usize * 8),
                ..data
            },
            "bit_size" => FieldMetadata {
                bit_size: Some(lit_int.value() as usize),
                ..data
            },
            _ => panic!("unsupported attribute: {}", name),
        },
        _ => panic!("unsupported attribute: {}", name),
    }
}

fn parse_doc(meta: &Meta, data: FieldMetadata) -> FieldMetadata {
    let mut data = data;
    if let Meta::NameValue(MetaNameValue {
        lit: Lit::Str(lit_str),
        ..
    }) = meta
    {
        data.docs += &lit_str.value();
        data.docs += "\n";
    }
    data
}

fn derive_field_used(fields: &[&Field]) -> Vec<TokenStream> {
    fields
        .iter()
        .map(|field| {
            let ident = &field.ident;
            quote! {
                // suppress the unused field warning
                std::ptr::eq(&field.#ident, &field.#ident);
            }
        })
        .collect()
}

fn derive_field_offset(
    generics: &ImplGenerics,
    fields: &[&Field],
    attrs: &[FieldMetadata],
) -> Vec<TokenStream> {
    fields
        .iter()
        .zip(attrs.iter())
        .map(|(field, attr)| {
            let ident = &field.ident;
            let ident_str = ident.as_ref().unwrap().to_string();
            let ty = &field.ty;
            let alias = &attr.alias;
            let kind = &attr.kind;
            let name = &attr.name;
            let description = &attr.description;
            let little_endian = &attr.little_endian;
            let bit_size = attr
                .bit_size
                .map(|size| quote! { Some(#size) })
                .unwrap_or(quote! { None });
            let var_ident = attr.var_ident.as_ref().map_or("x", String::as_ref);
            let func_decode = attr.func_decode.as_ref().map_or(var_ident, String::as_ref);
            let func_decode = replace_var_ident(TokenStream::from_str(&func_decode).unwrap(), var_ident);
            let input_ty = attr.input_ty.as_ref().map(|ty| {
                TokenStream::from_str(ty).unwrap()
            }).unwrap_or_else(|| {
                quote!{ <Alias #generics as genet_sdk::field::Field>::Output }
            });
            quote! {
                {
                    use genet_sdk::field::{FieldContext, Field, FieldType};
                    use genet_sdk::value::IntoOwnedValue;
                    type Alias #generics = #ty;
                    type Input #generics = #input_ty;
                    type Output #generics = <Alias #generics as genet_sdk::field::Field>::Output;
                    let offset = unsafe {
                        let val : Self = std::mem::uninitialized();
                        let offset = &val.#ident as *const Alias #generics as usize - &val as *const Self as usize;
                        std::mem::forget(val);
                        offset
                    };
                    let bit_range = bit_offset..(bit_offset + #bit_size.unwrap_or(Alias::bit_size()));
                    let mut field = FieldContext {
                        id: format!("{}.{}", parent_field.id, #ident_str).trim_matches('.').into(),
                        offset: parent_field.offset + offset,
                        bit_range: bit_range.clone(),
                        alias: #alias.into(),
                        kind: #kind.into(),
                        name: #name.into(),
                        description: #description.into(),
                        little_endian: #little_endian,
                        mapper: genet_sdk::mapper::MapperFunc::wrap(|value, data| {
                            {
                                let __value = value.clone().into_value(data);
                                let __value : &Input = __value.as_ref();
                                let __value : &Output = (&(#func_decode));
                                __value.into_owned_value(data)
                            }
                        }),
                        ..Default::default()
                    };
                    let field = FieldContext {
                        parser: Input::parser(&field),
                        ..field
                    };
                    let mut v = <Alias as genet_sdk::field::Field>::field_attrs(ctx, &field).into_iter().collect::<Vec<_>>();
                    let attr = genet_sdk::field::AttrType {
                        index: Some(<Alias as genet_sdk::field::Field>::field_index(parent_field.offset + offset)),
                        id: field.id.clone(),
                        bit_range: bit_range.clone(),
                        alias: field.alias.clone(),
                        kind: field.kind.clone(),
                        name: field.name.clone(),
                        description: field.description.clone(),
                        parser: genet_sdk::field::ParserBridge::new(Input::parser(&field)),
                        ..Default::default()
                    };
                    indices.push(attr);
                    indices.append(&mut v);
                    bit_offset = bit_range.end;
                }
            }
        })
        .collect()
}

fn derive_field_bit_size(
    generics: &ImplGenerics,
    fields: &[&Field],
    attrs: &[FieldMetadata],
) -> Vec<TokenStream> {
    fields
        .iter()
        .zip(attrs.iter())
        .map(|(field, _attr)| {
            let ty = &field.ty;
            quote! {
                {
                    type Alias #generics = #ty;
                    use genet_sdk::field::Field;
                    size += <Alias as Field>::bit_size();
                }
            }
        })
        .collect()
}

fn replace_var_ident(input: TokenStream, key: &str) -> TokenStream {
    let mut stream = TokenStream::new();
    for token in input.into_iter() {
        if let TokenTree::Ident(ident) = &token {
            if ident.to_string() == key {
                stream.extend(TokenStream::from_str("(*__value)"));
                continue;
            }
        }
        stream.extend(vec![token]);
    }
    stream
}

pub(crate) fn derive_attrs(input: &DeriveInput, data: &DataStruct) -> TokenStream {
    let ident = &input.ident;
    let struct_attr = StructMetadata::new(input);
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();

    if let Fields::Named(f) = &data.fields {
        let fields = f.named.iter().collect::<Vec<_>>();
        let attrs = fields
            .iter()
            .map(|&field| FieldMetadata::new(field))
            .collect::<Vec<_>>();

        let _field_used = derive_field_used(&fields);
        let field_size = derive_field_bit_size(&impl_generics, &fields, &attrs);
        let field_offset = derive_field_offset(&impl_generics, &fields, &attrs);

        let output_ty = struct_attr
            .ty
            .as_ref()
            .map(|ty| TokenStream::from_str(ty).unwrap())
            .unwrap_or_else(|| {
                quote! { [u8] }
            });

        let tokens = quote! {
            impl #impl_generics genet_sdk::field::Field for #ident #ty_generics #where_clause {
                type Output = #output_ty;

                fn field_attrs(ctx: &mut dyn genet_sdk::context::Context, parent_field: &genet_sdk::field::FieldContext) -> Vec<genet_sdk::field::AttrType> {
                    let mut indices = vec![];
                    let mut bit_offset = parent_field.bit_range.start;
                    #( #field_offset )*
                    indices
                }

                fn field_index(offset: usize) -> usize {
                    offset * 2 + 1
                }

                fn bit_size() -> usize {
                    let mut size = 0;
                    #( #field_size )*
                    size
                }
            }
        };
        tokens.into()
    } else {
        TokenStream::new()
    }
}
