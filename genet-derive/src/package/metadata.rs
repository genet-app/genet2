use syn::{Attribute, Lit, Meta, MetaList, MetaNameValue, NestedMeta};

#[derive(Debug, Default)]
pub struct FileType {
    pub name: String,
    pub extensions: Vec<String>,
}

#[derive(Debug, Default)]
pub struct ComponentMetadata {
    pub id: String,
    pub trigger_after: Vec<String>,
    pub files: Vec<FileType>,
}

impl ComponentMetadata {
    pub fn new(attrs: &[Attribute]) -> Self {
        attrs
            .iter()
            .filter_map(|attr| attr.interpret_meta())
            .fold(Default::default(), |data, meta| parse_meta(&meta, data))
    }
}

fn parse_meta(meta: &Meta, data: ComponentMetadata) -> ComponentMetadata {
    let name = meta.name().to_string();
    match (name.as_str(), meta) {
        ("decoder", Meta::List(list)) => parse_decoder(list, data),
        _ => data,
    }
}

fn parse_decoder(list: &MetaList, data: ComponentMetadata) -> ComponentMetadata {
    list.nested
        .iter()
        .filter_map(|item| {
            if let NestedMeta::Meta(meta) = item {
                Some(meta)
            } else {
                None
            }
        })
        .fold(data, |data, meta| parse_decoder_meta(meta, data))
}

fn parse_decoder_meta(meta: &Meta, data: ComponentMetadata) -> ComponentMetadata {
    let name = meta.name().to_string();
    match name.as_str() {
        "id" => {
            if let Meta::NameValue(MetaNameValue {
                lit: Lit::Str(lit_str),
                ..
            }) = meta
            {
                ComponentMetadata {
                    id: lit_str.value().trim().into(),
                    ..data
                }
            } else {
                panic!("id attribute must be a string")
            }
        }
        "after" => {
            let mut trigger_after = Vec::new();
            if let Meta::List(list) = meta {
                for item in &list.nested {
                    if let NestedMeta::Literal(Lit::Str(lit_str)) = item {
                        trigger_after.push(lit_str.value().trim().into());
                    }
                }
                ComponentMetadata {
                    trigger_after,
                    ..data
                }
            } else {
                panic!("after attribute must be a list")
            }
        }
        _ => panic!("unsupported attribute: {}", name),
    }
}
