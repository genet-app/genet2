use proc_macro::TokenStream;
use quote::*;
use syn::{parse_macro_input, Data, DeriveInput, Fields};

mod metadata;
use metadata::ComponentMetadata;

pub(crate) fn derive_package(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let data = if let Data::Struct(st) = &input.data {
        st
    } else {
        unimplemented!();
    };

    if let Fields::Named(f) = &data.fields {
        for field in &f.named {
            let meta = ComponentMetadata::new(&field.attrs);
            println!("{:?}", meta);
        }
    }

    let tokens = quote! {

        #[no_mangle]
        extern "C" fn genet_abi_v1_load_package() {

        }
    };
    tokens.into()
}
